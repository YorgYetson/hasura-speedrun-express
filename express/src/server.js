const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors')
const app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(cors({
  origin: '*',
  credentials: true
}))

const fetch = require("node-fetch")
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const PROD = process.env.ACCESS_TOKEN_SECRET == "production"
const ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET
const REFRESH_TOKEN_SECRET = process.env.REFRESH_TOKEN_SECRET
const ACCESS_DURATION = parseInt(process.env.ACCESS_DURATION)
const REFRESH_DURATION = parseInt(process.env.REFRESH_DURATION)
const HASURA_URL = process.env.HASURA_URL
const HASURA_GRAPHQL_ADMIN_SECRET = process.env.HASURA_GRAPHQL_ADMIN_SECRET


// SignUp Request Handler
app.post('/signup', async (req, res) => {
  const HASURA_OPERATION = `
    mutation ($interface: Int, $email: String, $password: String) {
      insert_Users_one(object:{
        interface:$interface,
        email:$email,
        password:$password
      }) {
        id
      }
    }
  `;

  // execute the parent operation in Hasura
  const execute = async (variables) => {
    const fetchResponse = await fetch(
      `http://${HASURA_URL}:8080/v1/graphql`,
      {
        method: 'POST',
        headers: { 'x-hasura-admin-secret': HASURA_GRAPHQL_ADMIN_SECRET },
        body: JSON.stringify({
          query: HASURA_OPERATION,
          variables
        })
      }
    );
    const data = await fetchResponse.json();
    console.log('DEBUG: ', data);
    return data;
  };

  // get request input
  const { interface, email, password } = req.body.input;

  // run some business logic
  let hashedPassword = await bcrypt.hash(password, 10);

  // execute the Hasura operation
  const { data, errors } = await execute({ interface, email, password: hashedPassword });

  // if Hasura operation errors, then throw error
  if (errors) {
    return res.status(400).json(errors[0])
  }

  // populate base token contents
  const tokenContents = {
    sub: data.insert_Users_one.id.toString(),
    interface: interface.toString(),
    email: email,
    iat: Date.now() / 1000,
    iss: 'https://myapp.com/',
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": ["user"],
      "x-hasura-user-id": data.insert_Users_one.id.toString(),
      "x-hasura-default-role": "user",
      "x-hasura-role": "user"
    }
  }

  // generate expiration dates for both tokens
  token_exp = Math.floor(Date.now() / 1000) + (ACCESS_DURATION)
  refresh_exp = Math.floor(Date.now() / 1000) + (REFRESH_DURATION)

  // time to make the donuts
  const token = jwt.sign({ ...tokenContents, exp: token_exp }, ACCESS_TOKEN_SECRET);
  const refresh = jwt.sign({ ...tokenContents, exp: refresh_exp }, REFRESH_TOKEN_SECRET);

  // put the refresh token in a cookie
  res.cookie('refreshToken', `${refresh}`, { SameSite: "None", maxAge: 86400 * 90, ...(PROD && { SameSite: "Secure", httpOnly: true, secure: true }) })

  // success
  return res.json({
    ...data.insert_Users_one,
    token: token
  })
});


// Login Request Handler
app.post('/login', async (req, res) => {
  console.log(req.body)
  console.log(req.body)
  console.log(req.body)

  const HASURA_OPERATION = `
  query ($interface: Int!, $email:String!){
    Users(where: {interface: {_eq: $interface}, email: {_eq: $email}}){
      id
      email
      password
    }
  }
  `;

  // execute the parent operation in Hasura
  const execute = async (variables) => {
    delete variables.password
    const fetchResponse = await fetch(
      `http://${HASURA_URL}:8080/v1/graphql`,
      {
        method: 'POST',
        headers: { 'x-hasura-admin-secret': HASURA_GRAPHQL_ADMIN_SECRET },
        body: JSON.stringify({
          query: HASURA_OPERATION,
          variables

        })
      }
    );
    const data = await fetchResponse.json();
    console.log('DEBUG: ', data);
    return data;
  };

  // get request input
  const { interface, email, password } = req.body.input;

  // execute the Hasura operation
  const { data, errors } = await execute({ interface, email, password });

  // if Hasura operation errors, then throw error
  if (errors) {
    return res.status(400).json(errors[0])
  }
  // run some business logic
  const verified = bcrypt.compareSync(password, data.Users[0].password);

  if (!verified) {
    return res.status(401).json({ "message": "Invalid Password" })
  }

  // populate base token contents
  const tokenContents = {
    sub: data.Users[0].id.toString(),
    interface: interface.toString(),
    email: email,
    iat: Date.now() / 1000,
    iss: 'https://myapp.com/',
    "https://hasura.io/jwt/claims": {
      "x-hasura-allowed-roles": ["user"],
      "x-hasura-user-id": data.Users[0].id.toString(),
      "x-hasura-default-role": "user",
      "x-hasura-role": "user"
    }
  }

  // generate expiration dates for both tokens
  token_exp = Math.floor(Date.now() / 1000) + (ACCESS_DURATION)
  refresh_exp = Math.floor(Date.now() / 1000) + (REFRESH_DURATION)

  // time to make the donuts
  const token = jwt.sign({ ...tokenContents, exp: token_exp }, ACCESS_TOKEN_SECRET);
  const refresh = jwt.sign({ ...tokenContents, exp: refresh_exp }, REFRESH_TOKEN_SECRET);

  // put the refresh token in a cookie
  res.cookie('refreshToken', `${refresh}`, { SameSite: "None", maxAge: 86400 * 90, ...(PROD && { SameSite: "Secure", httpOnly: true, secure: true }) })

  // success
  return res.json({
    token: token
  })
});


// Request Handler
app.post('/refresh', async (req, res) => {
  const HASURA_OPERATION = `
      query ($id: Int!){
      Users(where: {id: {_eq: $id}}) {
          id
          email
          interface
      }
      }
    `;

  // execute the parent operation in Hasura
  const execute = async (variables) => {
    const fetchResponse = await fetch(
      `http://${HASURA_URL}:8080/v1/graphql`,
      {
        method: 'POST',
        headers: { 'x-hasura-admin-secret': HASURA_GRAPHQL_ADMIN_SECRET },
        body: JSON.stringify({
          query: HASURA_OPERATION,
          variables
        })
      }
    );
    const data = await fetchResponse.json()
    console.log('DEBUG: ', data);
    return data;
  };

  // verify the refresh token
  let token;
  let tokenPayload;
  if (req.headers.cookie) {
    const rawCookies = req.headers.cookie.split('; ');
    for (let i = 0; i < rawCookies.length; i++) {
      if (rawCookies[i].startsWith("refreshToken")) {
        token = rawCookies[i].substring(13);
        tokenPayload = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString())
      }
    }
  }

  // check if refresh token exists
  if (!token) {
    return res.status(401).json({ error: "Refresh Token not found!" })
  }

  // check if refresh token is expired
  if (Date.now() >= tokenPayload.exp * 1000) {
    return res.status(401).json({ error: "Refresh Token has expired!" })
  }

  // verify the refresh token's signature
  if (jwt.verify(token, REFRESH_TOKEN_SECRET)) {
    // execute the Hasura operation
    const { data, errors } = await execute({ id: parseInt(tokenPayload.sub) });

    // if Hasura operation errors, then throw error
    if (errors) {
      return res.status(400).json(errors[0])
    }

    // populate base token contents
    const tokenContents = {
      sub: data.Users[0].id.toString(),
      interface: data.Users[0].interface.toString(),
      email: data.Users[0].email,
      iat: Date.now() / 1000,
      iss: 'https://myapp.com/',
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["user"],
        "x-hasura-user-id": data.Users[0].id.toString(),
        "x-hasura-default-role": "user",
        "x-hasura-role": "user"
      }
    }

    // generate expiration dates for new token
    token_exp = Math.floor(Date.now() / 1000) + (ACCESS_DURATION)

    // time to make the donuts
    const token = jwt.sign({ ...tokenContents, exp: token_exp }, ACCESS_TOKEN_SECRET);

    // success
    return res.json({
      token
    })
  } 
  return res.status(401).json({ error: "Refresh Token is not valid!" })
});

app.listen(PORT);


