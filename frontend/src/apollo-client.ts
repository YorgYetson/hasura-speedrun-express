import {
    ApolloClient,
    InMemoryCache,
    HttpLink,
    ApolloLink,
  } from '@apollo/client/core';
  import { WebSocketLink } from '@apollo/client/link/ws';
  import { getOperationAST } from 'graphql';
  import fetch from 'cross-fetch';
  import ws from "ws";
  const cache = new InMemoryCache({
    addTypename: true,
  });
  
  const wsLink = new WebSocketLink({
    uri: 'ws://http://localhost:8080/v1/graphql',
    webSocketImpl: ws,
    options: {
      lazy: true,
      reconnect: true,
    },
  });
  
  const httpLink = new HttpLink({
    uri: 'http://localhost:8080/v1/graphql',
    headers: {"x-hasura-admin-secret": "adminsecret"},
    fetch,
  });
  
  const link = ApolloLink.split(
    (op: any) => {
      // check if it is a subscription
      const operationAST = getOperationAST(op.query, op.operationName);
      return !!operationAST && operationAST.operation === 'subscription';
    },
    wsLink,
    httpLink
  );
  
  export default new ApolloClient({
    cache,
    link,
    connectToDevTools: true,
  });