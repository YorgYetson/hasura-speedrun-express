import client from './apollo-client';
import type {
  ApolloQueryResult,
  ObservableQuery,
  QueryOptions,
  MutationOptions,
} from '@apollo/client';
import { readable } from 'svelte/store';
import type { Readable } from 'svelte/store';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  timestamptz: any;
};

/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: Maybe<Scalars['Boolean']>;
  _gt?: Maybe<Scalars['Boolean']>;
  _gte?: Maybe<Scalars['Boolean']>;
  _in?: Maybe<Array<Scalars['Boolean']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Boolean']>;
  _lte?: Maybe<Scalars['Boolean']>;
  _neq?: Maybe<Scalars['Boolean']>;
  _nin?: Maybe<Array<Scalars['Boolean']>>;
};

/** columns and relationships of "EvemtPasswords" */
export type EvemtPasswords = {
  __typename?: 'EvemtPasswords';
  event: Scalars['Int'];
  id: Scalars['Int'];
  password: Scalars['String'];
};

/** aggregated selection of "EvemtPasswords" */
export type EvemtPasswords_Aggregate = {
  __typename?: 'EvemtPasswords_aggregate';
  aggregate?: Maybe<EvemtPasswords_Aggregate_Fields>;
  nodes: Array<EvemtPasswords>;
};

/** aggregate fields of "EvemtPasswords" */
export type EvemtPasswords_Aggregate_Fields = {
  __typename?: 'EvemtPasswords_aggregate_fields';
  avg?: Maybe<EvemtPasswords_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<EvemtPasswords_Max_Fields>;
  min?: Maybe<EvemtPasswords_Min_Fields>;
  stddev?: Maybe<EvemtPasswords_Stddev_Fields>;
  stddev_pop?: Maybe<EvemtPasswords_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<EvemtPasswords_Stddev_Samp_Fields>;
  sum?: Maybe<EvemtPasswords_Sum_Fields>;
  var_pop?: Maybe<EvemtPasswords_Var_Pop_Fields>;
  var_samp?: Maybe<EvemtPasswords_Var_Samp_Fields>;
  variance?: Maybe<EvemtPasswords_Variance_Fields>;
};

/** aggregate fields of "EvemtPasswords" */
export type EvemtPasswords_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<EvemtPasswords_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type EvemtPasswords_Avg_Fields = {
  __typename?: 'EvemtPasswords_avg_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "EvemtPasswords". All fields are combined with a logical 'AND'. */
export type EvemtPasswords_Bool_Exp = {
  _and?: Maybe<Array<EvemtPasswords_Bool_Exp>>;
  _not?: Maybe<EvemtPasswords_Bool_Exp>;
  _or?: Maybe<Array<EvemtPasswords_Bool_Exp>>;
  event?: Maybe<Int_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  password?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "EvemtPasswords" */
export enum EvemtPasswords_Constraint {
  /** unique or primary key constraint */
  EvemtPasswordsEventPasswordKey = 'EvemtPasswords_event_password_key',
  /** unique or primary key constraint */
  EvemtPasswordsPkey = 'EvemtPasswords_pkey',
}

/** input type for incrementing numeric columns in table "EvemtPasswords" */
export type EvemtPasswords_Inc_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "EvemtPasswords" */
export type EvemtPasswords_Insert_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type EvemtPasswords_Max_Fields = {
  __typename?: 'EvemtPasswords_max_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type EvemtPasswords_Min_Fields = {
  __typename?: 'EvemtPasswords_min_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "EvemtPasswords" */
export type EvemtPasswords_Mutation_Response = {
  __typename?: 'EvemtPasswords_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<EvemtPasswords>;
};

/** on conflict condition type for table "EvemtPasswords" */
export type EvemtPasswords_On_Conflict = {
  constraint: EvemtPasswords_Constraint;
  update_columns: Array<EvemtPasswords_Update_Column>;
  where?: Maybe<EvemtPasswords_Bool_Exp>;
};

/** Ordering options when selecting data from "EvemtPasswords". */
export type EvemtPasswords_Order_By = {
  event?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** primary key columns input for table: EvemtPasswords */
export type EvemtPasswords_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "EvemtPasswords" */
export enum EvemtPasswords_Select_Column {
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Password = 'password',
}

/** input type for updating data in table "EvemtPasswords" */
export type EvemtPasswords_Set_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type EvemtPasswords_Stddev_Fields = {
  __typename?: 'EvemtPasswords_stddev_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type EvemtPasswords_Stddev_Pop_Fields = {
  __typename?: 'EvemtPasswords_stddev_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type EvemtPasswords_Stddev_Samp_Fields = {
  __typename?: 'EvemtPasswords_stddev_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type EvemtPasswords_Sum_Fields = {
  __typename?: 'EvemtPasswords_sum_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
};

/** update columns of table "EvemtPasswords" */
export enum EvemtPasswords_Update_Column {
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Password = 'password',
}

/** aggregate var_pop on columns */
export type EvemtPasswords_Var_Pop_Fields = {
  __typename?: 'EvemtPasswords_var_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type EvemtPasswords_Var_Samp_Fields = {
  __typename?: 'EvemtPasswords_var_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type EvemtPasswords_Variance_Fields = {
  __typename?: 'EvemtPasswords_variance_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "EventPermissions" */
export type EventPermissions = {
  __typename?: 'EventPermissions';
  admin: Scalars['Boolean'];
  edit: Scalars['Boolean'];
  event: Scalars['Int'];
  id: Scalars['Int'];
  refund: Scalars['Boolean'];
  report: Scalars['Boolean'];
  user: Scalars['Int'];
};

/** aggregated selection of "EventPermissions" */
export type EventPermissions_Aggregate = {
  __typename?: 'EventPermissions_aggregate';
  aggregate?: Maybe<EventPermissions_Aggregate_Fields>;
  nodes: Array<EventPermissions>;
};

/** aggregate fields of "EventPermissions" */
export type EventPermissions_Aggregate_Fields = {
  __typename?: 'EventPermissions_aggregate_fields';
  avg?: Maybe<EventPermissions_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<EventPermissions_Max_Fields>;
  min?: Maybe<EventPermissions_Min_Fields>;
  stddev?: Maybe<EventPermissions_Stddev_Fields>;
  stddev_pop?: Maybe<EventPermissions_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<EventPermissions_Stddev_Samp_Fields>;
  sum?: Maybe<EventPermissions_Sum_Fields>;
  var_pop?: Maybe<EventPermissions_Var_Pop_Fields>;
  var_samp?: Maybe<EventPermissions_Var_Samp_Fields>;
  variance?: Maybe<EventPermissions_Variance_Fields>;
};

/** aggregate fields of "EventPermissions" */
export type EventPermissions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<EventPermissions_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type EventPermissions_Avg_Fields = {
  __typename?: 'EventPermissions_avg_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "EventPermissions". All fields are combined with a logical 'AND'. */
export type EventPermissions_Bool_Exp = {
  _and?: Maybe<Array<EventPermissions_Bool_Exp>>;
  _not?: Maybe<EventPermissions_Bool_Exp>;
  _or?: Maybe<Array<EventPermissions_Bool_Exp>>;
  admin?: Maybe<Boolean_Comparison_Exp>;
  edit?: Maybe<Boolean_Comparison_Exp>;
  event?: Maybe<Int_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  refund?: Maybe<Boolean_Comparison_Exp>;
  report?: Maybe<Boolean_Comparison_Exp>;
  user?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "EventPermissions" */
export enum EventPermissions_Constraint {
  /** unique or primary key constraint */
  EventPermissionsPkey = 'EventPermissions_pkey',
}

/** input type for incrementing numeric columns in table "EventPermissions" */
export type EventPermissions_Inc_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "EventPermissions" */
export type EventPermissions_Insert_Input = {
  admin?: Maybe<Scalars['Boolean']>;
  edit?: Maybe<Scalars['Boolean']>;
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  refund?: Maybe<Scalars['Boolean']>;
  report?: Maybe<Scalars['Boolean']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type EventPermissions_Max_Fields = {
  __typename?: 'EventPermissions_max_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type EventPermissions_Min_Fields = {
  __typename?: 'EventPermissions_min_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "EventPermissions" */
export type EventPermissions_Mutation_Response = {
  __typename?: 'EventPermissions_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<EventPermissions>;
};

/** on conflict condition type for table "EventPermissions" */
export type EventPermissions_On_Conflict = {
  constraint: EventPermissions_Constraint;
  update_columns: Array<EventPermissions_Update_Column>;
  where?: Maybe<EventPermissions_Bool_Exp>;
};

/** Ordering options when selecting data from "EventPermissions". */
export type EventPermissions_Order_By = {
  admin?: Maybe<Order_By>;
  edit?: Maybe<Order_By>;
  event?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  refund?: Maybe<Order_By>;
  report?: Maybe<Order_By>;
  user?: Maybe<Order_By>;
};

/** primary key columns input for table: EventPermissions */
export type EventPermissions_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "EventPermissions" */
export enum EventPermissions_Select_Column {
  /** column name */
  Admin = 'admin',
  /** column name */
  Edit = 'edit',
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Refund = 'refund',
  /** column name */
  Report = 'report',
  /** column name */
  User = 'user',
}

/** input type for updating data in table "EventPermissions" */
export type EventPermissions_Set_Input = {
  admin?: Maybe<Scalars['Boolean']>;
  edit?: Maybe<Scalars['Boolean']>;
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  refund?: Maybe<Scalars['Boolean']>;
  report?: Maybe<Scalars['Boolean']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type EventPermissions_Stddev_Fields = {
  __typename?: 'EventPermissions_stddev_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type EventPermissions_Stddev_Pop_Fields = {
  __typename?: 'EventPermissions_stddev_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type EventPermissions_Stddev_Samp_Fields = {
  __typename?: 'EventPermissions_stddev_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type EventPermissions_Sum_Fields = {
  __typename?: 'EventPermissions_sum_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** update columns of table "EventPermissions" */
export enum EventPermissions_Update_Column {
  /** column name */
  Admin = 'admin',
  /** column name */
  Edit = 'edit',
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Refund = 'refund',
  /** column name */
  Report = 'report',
  /** column name */
  User = 'user',
}

/** aggregate var_pop on columns */
export type EventPermissions_Var_Pop_Fields = {
  __typename?: 'EventPermissions_var_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type EventPermissions_Var_Samp_Fields = {
  __typename?: 'EventPermissions_var_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type EventPermissions_Variance_Fields = {
  __typename?: 'EventPermissions_variance_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "Events" */
export type Events = {
  __typename?: 'Events';
  description?: Maybe<Scalars['String']>;
  end_date: Scalars['timestamptz'];
  end_message?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  logo?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  organization: Scalars['Int'];
  owner: Scalars['Int'];
  parent?: Maybe<Scalars['Int']>;
  password_protected: Scalars['Boolean'];
  public: Scalars['Boolean'];
  publisjed: Scalars['Boolean'];
  start_date: Scalars['timestamptz'];
  timezone: Scalars['String'];
  type?: Maybe<Scalars['String']>;
  website: Scalars['String'];
};

/** aggregated selection of "Events" */
export type Events_Aggregate = {
  __typename?: 'Events_aggregate';
  aggregate?: Maybe<Events_Aggregate_Fields>;
  nodes: Array<Events>;
};

/** aggregate fields of "Events" */
export type Events_Aggregate_Fields = {
  __typename?: 'Events_aggregate_fields';
  avg?: Maybe<Events_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Events_Max_Fields>;
  min?: Maybe<Events_Min_Fields>;
  stddev?: Maybe<Events_Stddev_Fields>;
  stddev_pop?: Maybe<Events_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Events_Stddev_Samp_Fields>;
  sum?: Maybe<Events_Sum_Fields>;
  var_pop?: Maybe<Events_Var_Pop_Fields>;
  var_samp?: Maybe<Events_Var_Samp_Fields>;
  variance?: Maybe<Events_Variance_Fields>;
};

/** aggregate fields of "Events" */
export type Events_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Events_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Events_Avg_Fields = {
  __typename?: 'Events_avg_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "Events". All fields are combined with a logical 'AND'. */
export type Events_Bool_Exp = {
  _and?: Maybe<Array<Events_Bool_Exp>>;
  _not?: Maybe<Events_Bool_Exp>;
  _or?: Maybe<Array<Events_Bool_Exp>>;
  description?: Maybe<String_Comparison_Exp>;
  end_date?: Maybe<Timestamptz_Comparison_Exp>;
  end_message?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  logo?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  organization?: Maybe<Int_Comparison_Exp>;
  owner?: Maybe<Int_Comparison_Exp>;
  parent?: Maybe<Int_Comparison_Exp>;
  password_protected?: Maybe<Boolean_Comparison_Exp>;
  public?: Maybe<Boolean_Comparison_Exp>;
  publisjed?: Maybe<Boolean_Comparison_Exp>;
  start_date?: Maybe<Timestamptz_Comparison_Exp>;
  timezone?: Maybe<String_Comparison_Exp>;
  type?: Maybe<String_Comparison_Exp>;
  website?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "Events" */
export enum Events_Constraint {
  /** unique or primary key constraint */
  EventsPkey = 'Events_pkey',
}

/** input type for incrementing numeric columns in table "Events" */
export type Events_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "Events" */
export type Events_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['timestamptz']>;
  end_message?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
  password_protected?: Maybe<Scalars['Boolean']>;
  public?: Maybe<Scalars['Boolean']>;
  publisjed?: Maybe<Scalars['Boolean']>;
  start_date?: Maybe<Scalars['timestamptz']>;
  timezone?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Events_Max_Fields = {
  __typename?: 'Events_max_fields';
  description?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['timestamptz']>;
  end_message?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
  start_date?: Maybe<Scalars['timestamptz']>;
  timezone?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Events_Min_Fields = {
  __typename?: 'Events_min_fields';
  description?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['timestamptz']>;
  end_message?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
  start_date?: Maybe<Scalars['timestamptz']>;
  timezone?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "Events" */
export type Events_Mutation_Response = {
  __typename?: 'Events_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Events>;
};

/** on conflict condition type for table "Events" */
export type Events_On_Conflict = {
  constraint: Events_Constraint;
  update_columns: Array<Events_Update_Column>;
  where?: Maybe<Events_Bool_Exp>;
};

/** Ordering options when selecting data from "Events". */
export type Events_Order_By = {
  description?: Maybe<Order_By>;
  end_date?: Maybe<Order_By>;
  end_message?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  logo?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  organization?: Maybe<Order_By>;
  owner?: Maybe<Order_By>;
  parent?: Maybe<Order_By>;
  password_protected?: Maybe<Order_By>;
  public?: Maybe<Order_By>;
  publisjed?: Maybe<Order_By>;
  start_date?: Maybe<Order_By>;
  timezone?: Maybe<Order_By>;
  type?: Maybe<Order_By>;
  website?: Maybe<Order_By>;
};

/** primary key columns input for table: Events */
export type Events_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "Events" */
export enum Events_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  EndDate = 'end_date',
  /** column name */
  EndMessage = 'end_message',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
  /** column name */
  Owner = 'owner',
  /** column name */
  Parent = 'parent',
  /** column name */
  PasswordProtected = 'password_protected',
  /** column name */
  Public = 'public',
  /** column name */
  Publisjed = 'publisjed',
  /** column name */
  StartDate = 'start_date',
  /** column name */
  Timezone = 'timezone',
  /** column name */
  Type = 'type',
  /** column name */
  Website = 'website',
}

/** input type for updating data in table "Events" */
export type Events_Set_Input = {
  description?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['timestamptz']>;
  end_message?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
  password_protected?: Maybe<Scalars['Boolean']>;
  public?: Maybe<Scalars['Boolean']>;
  publisjed?: Maybe<Scalars['Boolean']>;
  start_date?: Maybe<Scalars['timestamptz']>;
  timezone?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Events_Stddev_Fields = {
  __typename?: 'Events_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Events_Stddev_Pop_Fields = {
  __typename?: 'Events_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Events_Stddev_Samp_Fields = {
  __typename?: 'Events_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type Events_Sum_Fields = {
  __typename?: 'Events_sum_fields';
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['Int']>;
};

/** update columns of table "Events" */
export enum Events_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  EndDate = 'end_date',
  /** column name */
  EndMessage = 'end_message',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
  /** column name */
  Owner = 'owner',
  /** column name */
  Parent = 'parent',
  /** column name */
  PasswordProtected = 'password_protected',
  /** column name */
  Public = 'public',
  /** column name */
  Publisjed = 'publisjed',
  /** column name */
  StartDate = 'start_date',
  /** column name */
  Timezone = 'timezone',
  /** column name */
  Type = 'type',
  /** column name */
  Website = 'website',
}

/** aggregate var_pop on columns */
export type Events_Var_Pop_Fields = {
  __typename?: 'Events_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Events_Var_Samp_Fields = {
  __typename?: 'Events_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Events_Variance_Fields = {
  __typename?: 'Events_variance_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
  parent?: Maybe<Scalars['Float']>;
};

/** Boolean expression to compare columns of type "Int". All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** columns and relationships of "InterfaceSubdomains" */
export type InterfaceSubdomains = {
  __typename?: 'InterfaceSubdomains';
  event?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  interface: Scalars['Int'];
  name: Scalars['String'];
  organization?: Maybe<Scalars['Int']>;
};

/** aggregated selection of "InterfaceSubdomains" */
export type InterfaceSubdomains_Aggregate = {
  __typename?: 'InterfaceSubdomains_aggregate';
  aggregate?: Maybe<InterfaceSubdomains_Aggregate_Fields>;
  nodes: Array<InterfaceSubdomains>;
};

/** aggregate fields of "InterfaceSubdomains" */
export type InterfaceSubdomains_Aggregate_Fields = {
  __typename?: 'InterfaceSubdomains_aggregate_fields';
  avg?: Maybe<InterfaceSubdomains_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<InterfaceSubdomains_Max_Fields>;
  min?: Maybe<InterfaceSubdomains_Min_Fields>;
  stddev?: Maybe<InterfaceSubdomains_Stddev_Fields>;
  stddev_pop?: Maybe<InterfaceSubdomains_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<InterfaceSubdomains_Stddev_Samp_Fields>;
  sum?: Maybe<InterfaceSubdomains_Sum_Fields>;
  var_pop?: Maybe<InterfaceSubdomains_Var_Pop_Fields>;
  var_samp?: Maybe<InterfaceSubdomains_Var_Samp_Fields>;
  variance?: Maybe<InterfaceSubdomains_Variance_Fields>;
};

/** aggregate fields of "InterfaceSubdomains" */
export type InterfaceSubdomains_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<InterfaceSubdomains_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type InterfaceSubdomains_Avg_Fields = {
  __typename?: 'InterfaceSubdomains_avg_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "InterfaceSubdomains". All fields are combined with a logical 'AND'. */
export type InterfaceSubdomains_Bool_Exp = {
  _and?: Maybe<Array<InterfaceSubdomains_Bool_Exp>>;
  _not?: Maybe<InterfaceSubdomains_Bool_Exp>;
  _or?: Maybe<Array<InterfaceSubdomains_Bool_Exp>>;
  event?: Maybe<Int_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  interface?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  organization?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "InterfaceSubdomains" */
export enum InterfaceSubdomains_Constraint {
  /** unique or primary key constraint */
  InterfaceSubdomainsInterfaceNameKey = 'InterfaceSubdomains_interface_name_key',
  /** unique or primary key constraint */
  InterfaceSubdomainsPkey = 'InterfaceSubdomains_pkey',
}

/** input type for incrementing numeric columns in table "InterfaceSubdomains" */
export type InterfaceSubdomains_Inc_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "InterfaceSubdomains" */
export type InterfaceSubdomains_Insert_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type InterfaceSubdomains_Max_Fields = {
  __typename?: 'InterfaceSubdomains_max_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type InterfaceSubdomains_Min_Fields = {
  __typename?: 'InterfaceSubdomains_min_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "InterfaceSubdomains" */
export type InterfaceSubdomains_Mutation_Response = {
  __typename?: 'InterfaceSubdomains_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<InterfaceSubdomains>;
};

/** on conflict condition type for table "InterfaceSubdomains" */
export type InterfaceSubdomains_On_Conflict = {
  constraint: InterfaceSubdomains_Constraint;
  update_columns: Array<InterfaceSubdomains_Update_Column>;
  where?: Maybe<InterfaceSubdomains_Bool_Exp>;
};

/** Ordering options when selecting data from "InterfaceSubdomains". */
export type InterfaceSubdomains_Order_By = {
  event?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  interface?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  organization?: Maybe<Order_By>;
};

/** primary key columns input for table: InterfaceSubdomains */
export type InterfaceSubdomains_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "InterfaceSubdomains" */
export enum InterfaceSubdomains_Select_Column {
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
}

/** input type for updating data in table "InterfaceSubdomains" */
export type InterfaceSubdomains_Set_Input = {
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type InterfaceSubdomains_Stddev_Fields = {
  __typename?: 'InterfaceSubdomains_stddev_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type InterfaceSubdomains_Stddev_Pop_Fields = {
  __typename?: 'InterfaceSubdomains_stddev_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type InterfaceSubdomains_Stddev_Samp_Fields = {
  __typename?: 'InterfaceSubdomains_stddev_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type InterfaceSubdomains_Sum_Fields = {
  __typename?: 'InterfaceSubdomains_sum_fields';
  event?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
};

/** update columns of table "InterfaceSubdomains" */
export enum InterfaceSubdomains_Update_Column {
  /** column name */
  Event = 'event',
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
}

/** aggregate var_pop on columns */
export type InterfaceSubdomains_Var_Pop_Fields = {
  __typename?: 'InterfaceSubdomains_var_pop_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type InterfaceSubdomains_Var_Samp_Fields = {
  __typename?: 'InterfaceSubdomains_var_samp_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type InterfaceSubdomains_Variance_Fields = {
  __typename?: 'InterfaceSubdomains_variance_fields';
  event?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "InterfaceUrls" */
export type InterfaceUrls = {
  __typename?: 'InterfaceUrls';
  id: Scalars['Int'];
  interface: Scalars['Int'];
  url: Scalars['String'];
};

/** aggregated selection of "InterfaceUrls" */
export type InterfaceUrls_Aggregate = {
  __typename?: 'InterfaceUrls_aggregate';
  aggregate?: Maybe<InterfaceUrls_Aggregate_Fields>;
  nodes: Array<InterfaceUrls>;
};

/** aggregate fields of "InterfaceUrls" */
export type InterfaceUrls_Aggregate_Fields = {
  __typename?: 'InterfaceUrls_aggregate_fields';
  avg?: Maybe<InterfaceUrls_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<InterfaceUrls_Max_Fields>;
  min?: Maybe<InterfaceUrls_Min_Fields>;
  stddev?: Maybe<InterfaceUrls_Stddev_Fields>;
  stddev_pop?: Maybe<InterfaceUrls_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<InterfaceUrls_Stddev_Samp_Fields>;
  sum?: Maybe<InterfaceUrls_Sum_Fields>;
  var_pop?: Maybe<InterfaceUrls_Var_Pop_Fields>;
  var_samp?: Maybe<InterfaceUrls_Var_Samp_Fields>;
  variance?: Maybe<InterfaceUrls_Variance_Fields>;
};

/** aggregate fields of "InterfaceUrls" */
export type InterfaceUrls_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<InterfaceUrls_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type InterfaceUrls_Avg_Fields = {
  __typename?: 'InterfaceUrls_avg_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "InterfaceUrls". All fields are combined with a logical 'AND'. */
export type InterfaceUrls_Bool_Exp = {
  _and?: Maybe<Array<InterfaceUrls_Bool_Exp>>;
  _not?: Maybe<InterfaceUrls_Bool_Exp>;
  _or?: Maybe<Array<InterfaceUrls_Bool_Exp>>;
  id?: Maybe<Int_Comparison_Exp>;
  interface?: Maybe<Int_Comparison_Exp>;
  url?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "InterfaceUrls" */
export enum InterfaceUrls_Constraint {
  /** unique or primary key constraint */
  InterfaceUrlsPkey = 'InterfaceUrls_pkey',
  /** unique or primary key constraint */
  InterfaceUrlsUrlKey = 'InterfaceUrls_url_key',
}

/** input type for incrementing numeric columns in table "InterfaceUrls" */
export type InterfaceUrls_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "InterfaceUrls" */
export type InterfaceUrls_Insert_Input = {
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type InterfaceUrls_Max_Fields = {
  __typename?: 'InterfaceUrls_max_fields';
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type InterfaceUrls_Min_Fields = {
  __typename?: 'InterfaceUrls_min_fields';
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "InterfaceUrls" */
export type InterfaceUrls_Mutation_Response = {
  __typename?: 'InterfaceUrls_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<InterfaceUrls>;
};

/** on conflict condition type for table "InterfaceUrls" */
export type InterfaceUrls_On_Conflict = {
  constraint: InterfaceUrls_Constraint;
  update_columns: Array<InterfaceUrls_Update_Column>;
  where?: Maybe<InterfaceUrls_Bool_Exp>;
};

/** Ordering options when selecting data from "InterfaceUrls". */
export type InterfaceUrls_Order_By = {
  id?: Maybe<Order_By>;
  interface?: Maybe<Order_By>;
  url?: Maybe<Order_By>;
};

/** primary key columns input for table: InterfaceUrls */
export type InterfaceUrls_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "InterfaceUrls" */
export enum InterfaceUrls_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Url = 'url',
}

/** input type for updating data in table "InterfaceUrls" */
export type InterfaceUrls_Set_Input = {
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  url?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type InterfaceUrls_Stddev_Fields = {
  __typename?: 'InterfaceUrls_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type InterfaceUrls_Stddev_Pop_Fields = {
  __typename?: 'InterfaceUrls_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type InterfaceUrls_Stddev_Samp_Fields = {
  __typename?: 'InterfaceUrls_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type InterfaceUrls_Sum_Fields = {
  __typename?: 'InterfaceUrls_sum_fields';
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
};

/** update columns of table "InterfaceUrls" */
export enum InterfaceUrls_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Url = 'url',
}

/** aggregate var_pop on columns */
export type InterfaceUrls_Var_Pop_Fields = {
  __typename?: 'InterfaceUrls_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type InterfaceUrls_Var_Samp_Fields = {
  __typename?: 'InterfaceUrls_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type InterfaceUrls_Variance_Fields = {
  __typename?: 'InterfaceUrls_variance_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "Interfaces" */
export type Interfaces = {
  __typename?: 'Interfaces';
  description: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
};

/** aggregated selection of "Interfaces" */
export type Interfaces_Aggregate = {
  __typename?: 'Interfaces_aggregate';
  aggregate?: Maybe<Interfaces_Aggregate_Fields>;
  nodes: Array<Interfaces>;
};

/** aggregate fields of "Interfaces" */
export type Interfaces_Aggregate_Fields = {
  __typename?: 'Interfaces_aggregate_fields';
  avg?: Maybe<Interfaces_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Interfaces_Max_Fields>;
  min?: Maybe<Interfaces_Min_Fields>;
  stddev?: Maybe<Interfaces_Stddev_Fields>;
  stddev_pop?: Maybe<Interfaces_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Interfaces_Stddev_Samp_Fields>;
  sum?: Maybe<Interfaces_Sum_Fields>;
  var_pop?: Maybe<Interfaces_Var_Pop_Fields>;
  var_samp?: Maybe<Interfaces_Var_Samp_Fields>;
  variance?: Maybe<Interfaces_Variance_Fields>;
};

/** aggregate fields of "Interfaces" */
export type Interfaces_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Interfaces_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Interfaces_Avg_Fields = {
  __typename?: 'Interfaces_avg_fields';
  id?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "Interfaces". All fields are combined with a logical 'AND'. */
export type Interfaces_Bool_Exp = {
  _and?: Maybe<Array<Interfaces_Bool_Exp>>;
  _not?: Maybe<Interfaces_Bool_Exp>;
  _or?: Maybe<Array<Interfaces_Bool_Exp>>;
  description?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "Interfaces" */
export enum Interfaces_Constraint {
  /** unique or primary key constraint */
  InterfacesNameKey = 'Interfaces_name_key',
  /** unique or primary key constraint */
  InterfacesPkey = 'Interfaces_pkey',
}

/** input type for incrementing numeric columns in table "Interfaces" */
export type Interfaces_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "Interfaces" */
export type Interfaces_Insert_Input = {
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Interfaces_Max_Fields = {
  __typename?: 'Interfaces_max_fields';
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Interfaces_Min_Fields = {
  __typename?: 'Interfaces_min_fields';
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "Interfaces" */
export type Interfaces_Mutation_Response = {
  __typename?: 'Interfaces_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Interfaces>;
};

/** on conflict condition type for table "Interfaces" */
export type Interfaces_On_Conflict = {
  constraint: Interfaces_Constraint;
  update_columns: Array<Interfaces_Update_Column>;
  where?: Maybe<Interfaces_Bool_Exp>;
};

/** Ordering options when selecting data from "Interfaces". */
export type Interfaces_Order_By = {
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
};

/** primary key columns input for table: Interfaces */
export type Interfaces_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "Interfaces" */
export enum Interfaces_Select_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
}

/** input type for updating data in table "Interfaces" */
export type Interfaces_Set_Input = {
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Interfaces_Stddev_Fields = {
  __typename?: 'Interfaces_stddev_fields';
  id?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Interfaces_Stddev_Pop_Fields = {
  __typename?: 'Interfaces_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Interfaces_Stddev_Samp_Fields = {
  __typename?: 'Interfaces_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type Interfaces_Sum_Fields = {
  __typename?: 'Interfaces_sum_fields';
  id?: Maybe<Scalars['Int']>;
};

/** update columns of table "Interfaces" */
export enum Interfaces_Update_Column {
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
}

/** aggregate var_pop on columns */
export type Interfaces_Var_Pop_Fields = {
  __typename?: 'Interfaces_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Interfaces_Var_Samp_Fields = {
  __typename?: 'Interfaces_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Interfaces_Variance_Fields = {
  __typename?: 'Interfaces_variance_fields';
  id?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "OrganizationImages" */
export type OrganizationImages = {
  __typename?: 'OrganizationImages';
  id: Scalars['Int'];
  location: Scalars['String'];
  name: Scalars['String'];
  organization: Scalars['Int'];
};

/** aggregated selection of "OrganizationImages" */
export type OrganizationImages_Aggregate = {
  __typename?: 'OrganizationImages_aggregate';
  aggregate?: Maybe<OrganizationImages_Aggregate_Fields>;
  nodes: Array<OrganizationImages>;
};

/** aggregate fields of "OrganizationImages" */
export type OrganizationImages_Aggregate_Fields = {
  __typename?: 'OrganizationImages_aggregate_fields';
  avg?: Maybe<OrganizationImages_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<OrganizationImages_Max_Fields>;
  min?: Maybe<OrganizationImages_Min_Fields>;
  stddev?: Maybe<OrganizationImages_Stddev_Fields>;
  stddev_pop?: Maybe<OrganizationImages_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<OrganizationImages_Stddev_Samp_Fields>;
  sum?: Maybe<OrganizationImages_Sum_Fields>;
  var_pop?: Maybe<OrganizationImages_Var_Pop_Fields>;
  var_samp?: Maybe<OrganizationImages_Var_Samp_Fields>;
  variance?: Maybe<OrganizationImages_Variance_Fields>;
};

/** aggregate fields of "OrganizationImages" */
export type OrganizationImages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<OrganizationImages_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type OrganizationImages_Avg_Fields = {
  __typename?: 'OrganizationImages_avg_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "OrganizationImages". All fields are combined with a logical 'AND'. */
export type OrganizationImages_Bool_Exp = {
  _and?: Maybe<Array<OrganizationImages_Bool_Exp>>;
  _not?: Maybe<OrganizationImages_Bool_Exp>;
  _or?: Maybe<Array<OrganizationImages_Bool_Exp>>;
  id?: Maybe<Int_Comparison_Exp>;
  location?: Maybe<String_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  organization?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "OrganizationImages" */
export enum OrganizationImages_Constraint {
  /** unique or primary key constraint */
  OrganizationImagesPkey = 'OrganizationImages_pkey',
}

/** input type for incrementing numeric columns in table "OrganizationImages" */
export type OrganizationImages_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "OrganizationImages" */
export type OrganizationImages_Insert_Input = {
  id?: Maybe<Scalars['Int']>;
  location?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type OrganizationImages_Max_Fields = {
  __typename?: 'OrganizationImages_max_fields';
  id?: Maybe<Scalars['Int']>;
  location?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type OrganizationImages_Min_Fields = {
  __typename?: 'OrganizationImages_min_fields';
  id?: Maybe<Scalars['Int']>;
  location?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "OrganizationImages" */
export type OrganizationImages_Mutation_Response = {
  __typename?: 'OrganizationImages_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<OrganizationImages>;
};

/** on conflict condition type for table "OrganizationImages" */
export type OrganizationImages_On_Conflict = {
  constraint: OrganizationImages_Constraint;
  update_columns: Array<OrganizationImages_Update_Column>;
  where?: Maybe<OrganizationImages_Bool_Exp>;
};

/** Ordering options when selecting data from "OrganizationImages". */
export type OrganizationImages_Order_By = {
  id?: Maybe<Order_By>;
  location?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  organization?: Maybe<Order_By>;
};

/** primary key columns input for table: OrganizationImages */
export type OrganizationImages_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "OrganizationImages" */
export enum OrganizationImages_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Location = 'location',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
}

/** input type for updating data in table "OrganizationImages" */
export type OrganizationImages_Set_Input = {
  id?: Maybe<Scalars['Int']>;
  location?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type OrganizationImages_Stddev_Fields = {
  __typename?: 'OrganizationImages_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type OrganizationImages_Stddev_Pop_Fields = {
  __typename?: 'OrganizationImages_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type OrganizationImages_Stddev_Samp_Fields = {
  __typename?: 'OrganizationImages_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type OrganizationImages_Sum_Fields = {
  __typename?: 'OrganizationImages_sum_fields';
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
};

/** update columns of table "OrganizationImages" */
export enum OrganizationImages_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Location = 'location',
  /** column name */
  Name = 'name',
  /** column name */
  Organization = 'organization',
}

/** aggregate var_pop on columns */
export type OrganizationImages_Var_Pop_Fields = {
  __typename?: 'OrganizationImages_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type OrganizationImages_Var_Samp_Fields = {
  __typename?: 'OrganizationImages_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type OrganizationImages_Variance_Fields = {
  __typename?: 'OrganizationImages_variance_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "OrganizationPermissions" */
export type OrganizationPermissions = {
  __typename?: 'OrganizationPermissions';
  admin: Scalars['Boolean'];
  edit: Scalars['Boolean'];
  id: Scalars['Int'];
  organization: Scalars['Int'];
  refund: Scalars['Boolean'];
  report: Scalars['Boolean'];
  user: Scalars['Int'];
};

/** aggregated selection of "OrganizationPermissions" */
export type OrganizationPermissions_Aggregate = {
  __typename?: 'OrganizationPermissions_aggregate';
  aggregate?: Maybe<OrganizationPermissions_Aggregate_Fields>;
  nodes: Array<OrganizationPermissions>;
};

/** aggregate fields of "OrganizationPermissions" */
export type OrganizationPermissions_Aggregate_Fields = {
  __typename?: 'OrganizationPermissions_aggregate_fields';
  avg?: Maybe<OrganizationPermissions_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<OrganizationPermissions_Max_Fields>;
  min?: Maybe<OrganizationPermissions_Min_Fields>;
  stddev?: Maybe<OrganizationPermissions_Stddev_Fields>;
  stddev_pop?: Maybe<OrganizationPermissions_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<OrganizationPermissions_Stddev_Samp_Fields>;
  sum?: Maybe<OrganizationPermissions_Sum_Fields>;
  var_pop?: Maybe<OrganizationPermissions_Var_Pop_Fields>;
  var_samp?: Maybe<OrganizationPermissions_Var_Samp_Fields>;
  variance?: Maybe<OrganizationPermissions_Variance_Fields>;
};

/** aggregate fields of "OrganizationPermissions" */
export type OrganizationPermissions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<OrganizationPermissions_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type OrganizationPermissions_Avg_Fields = {
  __typename?: 'OrganizationPermissions_avg_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "OrganizationPermissions". All fields are combined with a logical 'AND'. */
export type OrganizationPermissions_Bool_Exp = {
  _and?: Maybe<Array<OrganizationPermissions_Bool_Exp>>;
  _not?: Maybe<OrganizationPermissions_Bool_Exp>;
  _or?: Maybe<Array<OrganizationPermissions_Bool_Exp>>;
  admin?: Maybe<Boolean_Comparison_Exp>;
  edit?: Maybe<Boolean_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  organization?: Maybe<Int_Comparison_Exp>;
  refund?: Maybe<Boolean_Comparison_Exp>;
  report?: Maybe<Boolean_Comparison_Exp>;
  user?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "OrganizationPermissions" */
export enum OrganizationPermissions_Constraint {
  /** unique or primary key constraint */
  OrganizationPermissionsPkey = 'OrganizationPermissions_pkey',
  /** unique or primary key constraint */
  OrganizationPermissionsUserOrganizationKey = 'OrganizationPermissions_user_organization_key',
}

/** input type for incrementing numeric columns in table "OrganizationPermissions" */
export type OrganizationPermissions_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "OrganizationPermissions" */
export type OrganizationPermissions_Insert_Input = {
  admin?: Maybe<Scalars['Boolean']>;
  edit?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  refund?: Maybe<Scalars['Boolean']>;
  report?: Maybe<Scalars['Boolean']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type OrganizationPermissions_Max_Fields = {
  __typename?: 'OrganizationPermissions_max_fields';
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type OrganizationPermissions_Min_Fields = {
  __typename?: 'OrganizationPermissions_min_fields';
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "OrganizationPermissions" */
export type OrganizationPermissions_Mutation_Response = {
  __typename?: 'OrganizationPermissions_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<OrganizationPermissions>;
};

/** on conflict condition type for table "OrganizationPermissions" */
export type OrganizationPermissions_On_Conflict = {
  constraint: OrganizationPermissions_Constraint;
  update_columns: Array<OrganizationPermissions_Update_Column>;
  where?: Maybe<OrganizationPermissions_Bool_Exp>;
};

/** Ordering options when selecting data from "OrganizationPermissions". */
export type OrganizationPermissions_Order_By = {
  admin?: Maybe<Order_By>;
  edit?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  organization?: Maybe<Order_By>;
  refund?: Maybe<Order_By>;
  report?: Maybe<Order_By>;
  user?: Maybe<Order_By>;
};

/** primary key columns input for table: OrganizationPermissions */
export type OrganizationPermissions_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "OrganizationPermissions" */
export enum OrganizationPermissions_Select_Column {
  /** column name */
  Admin = 'admin',
  /** column name */
  Edit = 'edit',
  /** column name */
  Id = 'id',
  /** column name */
  Organization = 'organization',
  /** column name */
  Refund = 'refund',
  /** column name */
  Report = 'report',
  /** column name */
  User = 'user',
}

/** input type for updating data in table "OrganizationPermissions" */
export type OrganizationPermissions_Set_Input = {
  admin?: Maybe<Scalars['Boolean']>;
  edit?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  refund?: Maybe<Scalars['Boolean']>;
  report?: Maybe<Scalars['Boolean']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type OrganizationPermissions_Stddev_Fields = {
  __typename?: 'OrganizationPermissions_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type OrganizationPermissions_Stddev_Pop_Fields = {
  __typename?: 'OrganizationPermissions_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type OrganizationPermissions_Stddev_Samp_Fields = {
  __typename?: 'OrganizationPermissions_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type OrganizationPermissions_Sum_Fields = {
  __typename?: 'OrganizationPermissions_sum_fields';
  id?: Maybe<Scalars['Int']>;
  organization?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** update columns of table "OrganizationPermissions" */
export enum OrganizationPermissions_Update_Column {
  /** column name */
  Admin = 'admin',
  /** column name */
  Edit = 'edit',
  /** column name */
  Id = 'id',
  /** column name */
  Organization = 'organization',
  /** column name */
  Refund = 'refund',
  /** column name */
  Report = 'report',
  /** column name */
  User = 'user',
}

/** aggregate var_pop on columns */
export type OrganizationPermissions_Var_Pop_Fields = {
  __typename?: 'OrganizationPermissions_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type OrganizationPermissions_Var_Samp_Fields = {
  __typename?: 'OrganizationPermissions_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type OrganizationPermissions_Variance_Fields = {
  __typename?: 'OrganizationPermissions_variance_fields';
  id?: Maybe<Scalars['Float']>;
  organization?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "Organizations" */
export type Organizations = {
  __typename?: 'Organizations';
  billing_address_line_one: Scalars['String'];
  billing_address_line_two?: Maybe<Scalars['String']>;
  billing_country: Scalars['String'];
  billing_country_other: Scalars['String'];
  billing_payable: Scalars['String'];
  billing_postal_code: Scalars['String'];
  contact_email: Scalars['String'];
  contact_first_name: Scalars['String'];
  contact_last_name: Scalars['String'];
  contact_phone: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  logo: Scalars['Int'];
  name: Scalars['String'];
  owner: Scalars['Int'];
  website: Scalars['String'];
};

/** aggregated selection of "Organizations" */
export type Organizations_Aggregate = {
  __typename?: 'Organizations_aggregate';
  aggregate?: Maybe<Organizations_Aggregate_Fields>;
  nodes: Array<Organizations>;
};

/** aggregate fields of "Organizations" */
export type Organizations_Aggregate_Fields = {
  __typename?: 'Organizations_aggregate_fields';
  avg?: Maybe<Organizations_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Organizations_Max_Fields>;
  min?: Maybe<Organizations_Min_Fields>;
  stddev?: Maybe<Organizations_Stddev_Fields>;
  stddev_pop?: Maybe<Organizations_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Organizations_Stddev_Samp_Fields>;
  sum?: Maybe<Organizations_Sum_Fields>;
  var_pop?: Maybe<Organizations_Var_Pop_Fields>;
  var_samp?: Maybe<Organizations_Var_Samp_Fields>;
  variance?: Maybe<Organizations_Variance_Fields>;
};

/** aggregate fields of "Organizations" */
export type Organizations_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Organizations_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Organizations_Avg_Fields = {
  __typename?: 'Organizations_avg_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "Organizations". All fields are combined with a logical 'AND'. */
export type Organizations_Bool_Exp = {
  _and?: Maybe<Array<Organizations_Bool_Exp>>;
  _not?: Maybe<Organizations_Bool_Exp>;
  _or?: Maybe<Array<Organizations_Bool_Exp>>;
  billing_address_line_one?: Maybe<String_Comparison_Exp>;
  billing_address_line_two?: Maybe<String_Comparison_Exp>;
  billing_country?: Maybe<String_Comparison_Exp>;
  billing_country_other?: Maybe<String_Comparison_Exp>;
  billing_payable?: Maybe<String_Comparison_Exp>;
  billing_postal_code?: Maybe<String_Comparison_Exp>;
  contact_email?: Maybe<String_Comparison_Exp>;
  contact_first_name?: Maybe<String_Comparison_Exp>;
  contact_last_name?: Maybe<String_Comparison_Exp>;
  contact_phone?: Maybe<String_Comparison_Exp>;
  description?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  logo?: Maybe<Int_Comparison_Exp>;
  name?: Maybe<String_Comparison_Exp>;
  owner?: Maybe<Int_Comparison_Exp>;
  website?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "Organizations" */
export enum Organizations_Constraint {
  /** unique or primary key constraint */
  OrganizationsPkey = 'Organizations_pkey',
}

/** input type for incrementing numeric columns in table "Organizations" */
export type Organizations_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "Organizations" */
export type Organizations_Insert_Input = {
  billing_address_line_one?: Maybe<Scalars['String']>;
  billing_address_line_two?: Maybe<Scalars['String']>;
  billing_country?: Maybe<Scalars['String']>;
  billing_country_other?: Maybe<Scalars['String']>;
  billing_payable?: Maybe<Scalars['String']>;
  billing_postal_code?: Maybe<Scalars['String']>;
  contact_email?: Maybe<Scalars['String']>;
  contact_first_name?: Maybe<Scalars['String']>;
  contact_last_name?: Maybe<Scalars['String']>;
  contact_phone?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['Int']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Organizations_Max_Fields = {
  __typename?: 'Organizations_max_fields';
  billing_address_line_one?: Maybe<Scalars['String']>;
  billing_address_line_two?: Maybe<Scalars['String']>;
  billing_country?: Maybe<Scalars['String']>;
  billing_country_other?: Maybe<Scalars['String']>;
  billing_payable?: Maybe<Scalars['String']>;
  billing_postal_code?: Maybe<Scalars['String']>;
  contact_email?: Maybe<Scalars['String']>;
  contact_first_name?: Maybe<Scalars['String']>;
  contact_last_name?: Maybe<Scalars['String']>;
  contact_phone?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['Int']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Organizations_Min_Fields = {
  __typename?: 'Organizations_min_fields';
  billing_address_line_one?: Maybe<Scalars['String']>;
  billing_address_line_two?: Maybe<Scalars['String']>;
  billing_country?: Maybe<Scalars['String']>;
  billing_country_other?: Maybe<Scalars['String']>;
  billing_payable?: Maybe<Scalars['String']>;
  billing_postal_code?: Maybe<Scalars['String']>;
  contact_email?: Maybe<Scalars['String']>;
  contact_first_name?: Maybe<Scalars['String']>;
  contact_last_name?: Maybe<Scalars['String']>;
  contact_phone?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['Int']>;
  website?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "Organizations" */
export type Organizations_Mutation_Response = {
  __typename?: 'Organizations_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Organizations>;
};

/** on conflict condition type for table "Organizations" */
export type Organizations_On_Conflict = {
  constraint: Organizations_Constraint;
  update_columns: Array<Organizations_Update_Column>;
  where?: Maybe<Organizations_Bool_Exp>;
};

/** Ordering options when selecting data from "Organizations". */
export type Organizations_Order_By = {
  billing_address_line_one?: Maybe<Order_By>;
  billing_address_line_two?: Maybe<Order_By>;
  billing_country?: Maybe<Order_By>;
  billing_country_other?: Maybe<Order_By>;
  billing_payable?: Maybe<Order_By>;
  billing_postal_code?: Maybe<Order_By>;
  contact_email?: Maybe<Order_By>;
  contact_first_name?: Maybe<Order_By>;
  contact_last_name?: Maybe<Order_By>;
  contact_phone?: Maybe<Order_By>;
  description?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  logo?: Maybe<Order_By>;
  name?: Maybe<Order_By>;
  owner?: Maybe<Order_By>;
  website?: Maybe<Order_By>;
};

/** primary key columns input for table: Organizations */
export type Organizations_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "Organizations" */
export enum Organizations_Select_Column {
  /** column name */
  BillingAddressLineOne = 'billing_address_line_one',
  /** column name */
  BillingAddressLineTwo = 'billing_address_line_two',
  /** column name */
  BillingCountry = 'billing_country',
  /** column name */
  BillingCountryOther = 'billing_country_other',
  /** column name */
  BillingPayable = 'billing_payable',
  /** column name */
  BillingPostalCode = 'billing_postal_code',
  /** column name */
  ContactEmail = 'contact_email',
  /** column name */
  ContactFirstName = 'contact_first_name',
  /** column name */
  ContactLastName = 'contact_last_name',
  /** column name */
  ContactPhone = 'contact_phone',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Owner = 'owner',
  /** column name */
  Website = 'website',
}

/** input type for updating data in table "Organizations" */
export type Organizations_Set_Input = {
  billing_address_line_one?: Maybe<Scalars['String']>;
  billing_address_line_two?: Maybe<Scalars['String']>;
  billing_country?: Maybe<Scalars['String']>;
  billing_country_other?: Maybe<Scalars['String']>;
  billing_payable?: Maybe<Scalars['String']>;
  billing_postal_code?: Maybe<Scalars['String']>;
  contact_email?: Maybe<Scalars['String']>;
  contact_first_name?: Maybe<Scalars['String']>;
  contact_last_name?: Maybe<Scalars['String']>;
  contact_phone?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<Scalars['Int']>;
  website?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Organizations_Stddev_Fields = {
  __typename?: 'Organizations_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Organizations_Stddev_Pop_Fields = {
  __typename?: 'Organizations_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Organizations_Stddev_Samp_Fields = {
  __typename?: 'Organizations_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type Organizations_Sum_Fields = {
  __typename?: 'Organizations_sum_fields';
  id?: Maybe<Scalars['Int']>;
  logo?: Maybe<Scalars['Int']>;
  owner?: Maybe<Scalars['Int']>;
};

/** update columns of table "Organizations" */
export enum Organizations_Update_Column {
  /** column name */
  BillingAddressLineOne = 'billing_address_line_one',
  /** column name */
  BillingAddressLineTwo = 'billing_address_line_two',
  /** column name */
  BillingCountry = 'billing_country',
  /** column name */
  BillingCountryOther = 'billing_country_other',
  /** column name */
  BillingPayable = 'billing_payable',
  /** column name */
  BillingPostalCode = 'billing_postal_code',
  /** column name */
  ContactEmail = 'contact_email',
  /** column name */
  ContactFirstName = 'contact_first_name',
  /** column name */
  ContactLastName = 'contact_last_name',
  /** column name */
  ContactPhone = 'contact_phone',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Logo = 'logo',
  /** column name */
  Name = 'name',
  /** column name */
  Owner = 'owner',
  /** column name */
  Website = 'website',
}

/** aggregate var_pop on columns */
export type Organizations_Var_Pop_Fields = {
  __typename?: 'Organizations_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Organizations_Var_Samp_Fields = {
  __typename?: 'Organizations_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Organizations_Variance_Fields = {
  __typename?: 'Organizations_variance_fields';
  id?: Maybe<Scalars['Float']>;
  logo?: Maybe<Scalars['Float']>;
  owner?: Maybe<Scalars['Float']>;
};

/** columns and relationships of "Profiles" */
export type Profiles = {
  __typename?: 'Profiles';
  address_line_1: Scalars['String'];
  address_line_2: Scalars['String'];
  age: Scalars['Int'];
  city: Scalars['String'];
  company: Scalars['String'];
  country: Scalars['String'];
  country_other: Scalars['String'];
  first_name: Scalars['String'];
  gender: Scalars['String'];
  id: Scalars['Int'];
  job_title: Scalars['String'];
  last_name: Scalars['String'];
  notes: Scalars['String'];
  phone: Scalars['String'];
  postal_code: Scalars['String'];
  state_province: Scalars['String'];
  user: Scalars['Int'];
};

/** aggregated selection of "Profiles" */
export type Profiles_Aggregate = {
  __typename?: 'Profiles_aggregate';
  aggregate?: Maybe<Profiles_Aggregate_Fields>;
  nodes: Array<Profiles>;
};

/** aggregate fields of "Profiles" */
export type Profiles_Aggregate_Fields = {
  __typename?: 'Profiles_aggregate_fields';
  avg?: Maybe<Profiles_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Profiles_Max_Fields>;
  min?: Maybe<Profiles_Min_Fields>;
  stddev?: Maybe<Profiles_Stddev_Fields>;
  stddev_pop?: Maybe<Profiles_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Profiles_Stddev_Samp_Fields>;
  sum?: Maybe<Profiles_Sum_Fields>;
  var_pop?: Maybe<Profiles_Var_Pop_Fields>;
  var_samp?: Maybe<Profiles_Var_Samp_Fields>;
  variance?: Maybe<Profiles_Variance_Fields>;
};

/** aggregate fields of "Profiles" */
export type Profiles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Profiles_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Profiles_Avg_Fields = {
  __typename?: 'Profiles_avg_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "Profiles". All fields are combined with a logical 'AND'. */
export type Profiles_Bool_Exp = {
  _and?: Maybe<Array<Profiles_Bool_Exp>>;
  _not?: Maybe<Profiles_Bool_Exp>;
  _or?: Maybe<Array<Profiles_Bool_Exp>>;
  address_line_1?: Maybe<String_Comparison_Exp>;
  address_line_2?: Maybe<String_Comparison_Exp>;
  age?: Maybe<Int_Comparison_Exp>;
  city?: Maybe<String_Comparison_Exp>;
  company?: Maybe<String_Comparison_Exp>;
  country?: Maybe<String_Comparison_Exp>;
  country_other?: Maybe<String_Comparison_Exp>;
  first_name?: Maybe<String_Comparison_Exp>;
  gender?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  job_title?: Maybe<String_Comparison_Exp>;
  last_name?: Maybe<String_Comparison_Exp>;
  notes?: Maybe<String_Comparison_Exp>;
  phone?: Maybe<String_Comparison_Exp>;
  postal_code?: Maybe<String_Comparison_Exp>;
  state_province?: Maybe<String_Comparison_Exp>;
  user?: Maybe<Int_Comparison_Exp>;
};

/** unique or primary key constraints on table "Profiles" */
export enum Profiles_Constraint {
  /** unique or primary key constraint */
  ProfilesPkey = 'Profiles_pkey',
  /** unique or primary key constraint */
  ProfilesUserKey = 'Profiles_user_key',
}

/** input type for incrementing numeric columns in table "Profiles" */
export type Profiles_Inc_Input = {
  age?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "Profiles" */
export type Profiles_Insert_Input = {
  address_line_1?: Maybe<Scalars['String']>;
  address_line_2?: Maybe<Scalars['String']>;
  age?: Maybe<Scalars['Int']>;
  city?: Maybe<Scalars['String']>;
  company?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  country_other?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  job_title?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  state_province?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type Profiles_Max_Fields = {
  __typename?: 'Profiles_max_fields';
  address_line_1?: Maybe<Scalars['String']>;
  address_line_2?: Maybe<Scalars['String']>;
  age?: Maybe<Scalars['Int']>;
  city?: Maybe<Scalars['String']>;
  company?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  country_other?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  job_title?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  state_province?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate min on columns */
export type Profiles_Min_Fields = {
  __typename?: 'Profiles_min_fields';
  address_line_1?: Maybe<Scalars['String']>;
  address_line_2?: Maybe<Scalars['String']>;
  age?: Maybe<Scalars['Int']>;
  city?: Maybe<Scalars['String']>;
  company?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  country_other?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  job_title?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  state_province?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['Int']>;
};

/** response of any mutation on the table "Profiles" */
export type Profiles_Mutation_Response = {
  __typename?: 'Profiles_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Profiles>;
};

/** on conflict condition type for table "Profiles" */
export type Profiles_On_Conflict = {
  constraint: Profiles_Constraint;
  update_columns: Array<Profiles_Update_Column>;
  where?: Maybe<Profiles_Bool_Exp>;
};

/** Ordering options when selecting data from "Profiles". */
export type Profiles_Order_By = {
  address_line_1?: Maybe<Order_By>;
  address_line_2?: Maybe<Order_By>;
  age?: Maybe<Order_By>;
  city?: Maybe<Order_By>;
  company?: Maybe<Order_By>;
  country?: Maybe<Order_By>;
  country_other?: Maybe<Order_By>;
  first_name?: Maybe<Order_By>;
  gender?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  job_title?: Maybe<Order_By>;
  last_name?: Maybe<Order_By>;
  notes?: Maybe<Order_By>;
  phone?: Maybe<Order_By>;
  postal_code?: Maybe<Order_By>;
  state_province?: Maybe<Order_By>;
  user?: Maybe<Order_By>;
};

/** primary key columns input for table: Profiles */
export type Profiles_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "Profiles" */
export enum Profiles_Select_Column {
  /** column name */
  AddressLine_1 = 'address_line_1',
  /** column name */
  AddressLine_2 = 'address_line_2',
  /** column name */
  Age = 'age',
  /** column name */
  City = 'city',
  /** column name */
  Company = 'company',
  /** column name */
  Country = 'country',
  /** column name */
  CountryOther = 'country_other',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Gender = 'gender',
  /** column name */
  Id = 'id',
  /** column name */
  JobTitle = 'job_title',
  /** column name */
  LastName = 'last_name',
  /** column name */
  Notes = 'notes',
  /** column name */
  Phone = 'phone',
  /** column name */
  PostalCode = 'postal_code',
  /** column name */
  StateProvince = 'state_province',
  /** column name */
  User = 'user',
}

/** input type for updating data in table "Profiles" */
export type Profiles_Set_Input = {
  address_line_1?: Maybe<Scalars['String']>;
  address_line_2?: Maybe<Scalars['String']>;
  age?: Maybe<Scalars['Int']>;
  city?: Maybe<Scalars['String']>;
  company?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  country_other?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  job_title?: Maybe<Scalars['String']>;
  last_name?: Maybe<Scalars['String']>;
  notes?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  state_province?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type Profiles_Stddev_Fields = {
  __typename?: 'Profiles_stddev_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Profiles_Stddev_Pop_Fields = {
  __typename?: 'Profiles_stddev_pop_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Profiles_Stddev_Samp_Fields = {
  __typename?: 'Profiles_stddev_samp_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type Profiles_Sum_Fields = {
  __typename?: 'Profiles_sum_fields';
  age?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['Int']>;
};

/** update columns of table "Profiles" */
export enum Profiles_Update_Column {
  /** column name */
  AddressLine_1 = 'address_line_1',
  /** column name */
  AddressLine_2 = 'address_line_2',
  /** column name */
  Age = 'age',
  /** column name */
  City = 'city',
  /** column name */
  Company = 'company',
  /** column name */
  Country = 'country',
  /** column name */
  CountryOther = 'country_other',
  /** column name */
  FirstName = 'first_name',
  /** column name */
  Gender = 'gender',
  /** column name */
  Id = 'id',
  /** column name */
  JobTitle = 'job_title',
  /** column name */
  LastName = 'last_name',
  /** column name */
  Notes = 'notes',
  /** column name */
  Phone = 'phone',
  /** column name */
  PostalCode = 'postal_code',
  /** column name */
  StateProvince = 'state_province',
  /** column name */
  User = 'user',
}

/** aggregate var_pop on columns */
export type Profiles_Var_Pop_Fields = {
  __typename?: 'Profiles_var_pop_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Profiles_Var_Samp_Fields = {
  __typename?: 'Profiles_var_samp_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Profiles_Variance_Fields = {
  __typename?: 'Profiles_variance_fields';
  age?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['Float']>;
  user?: Maybe<Scalars['Float']>;
};

/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  /** does the column match the given case-insensitive pattern */
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  /** does the column match the given POSIX regular expression, case insensitive */
  _iregex?: Maybe<Scalars['String']>;
  _is_null?: Maybe<Scalars['Boolean']>;
  /** does the column match the given pattern */
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  /** does the column NOT match the given case-insensitive pattern */
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  /** does the column NOT match the given POSIX regular expression, case insensitive */
  _niregex?: Maybe<Scalars['String']>;
  /** does the column NOT match the given pattern */
  _nlike?: Maybe<Scalars['String']>;
  /** does the column NOT match the given POSIX regular expression, case sensitive */
  _nregex?: Maybe<Scalars['String']>;
  /** does the column NOT match the given SQL regular expression */
  _nsimilar?: Maybe<Scalars['String']>;
  /** does the column match the given POSIX regular expression, case sensitive */
  _regex?: Maybe<Scalars['String']>;
  /** does the column match the given SQL regular expression */
  _similar?: Maybe<Scalars['String']>;
};

/** columns and relationships of "Users" */
export type Users = {
  __typename?: 'Users';
  email: Scalars['String'];
  id: Scalars['Int'];
  interface: Scalars['Int'];
  password: Scalars['String'];
};

/** aggregated selection of "Users" */
export type Users_Aggregate = {
  __typename?: 'Users_aggregate';
  aggregate?: Maybe<Users_Aggregate_Fields>;
  nodes: Array<Users>;
};

/** aggregate fields of "Users" */
export type Users_Aggregate_Fields = {
  __typename?: 'Users_aggregate_fields';
  avg?: Maybe<Users_Avg_Fields>;
  count: Scalars['Int'];
  max?: Maybe<Users_Max_Fields>;
  min?: Maybe<Users_Min_Fields>;
  stddev?: Maybe<Users_Stddev_Fields>;
  stddev_pop?: Maybe<Users_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Users_Stddev_Samp_Fields>;
  sum?: Maybe<Users_Sum_Fields>;
  var_pop?: Maybe<Users_Var_Pop_Fields>;
  var_samp?: Maybe<Users_Var_Samp_Fields>;
  variance?: Maybe<Users_Variance_Fields>;
};

/** aggregate fields of "Users" */
export type Users_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** aggregate avg on columns */
export type Users_Avg_Fields = {
  __typename?: 'Users_avg_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** Boolean expression to filter rows from the table "Users". All fields are combined with a logical 'AND'. */
export type Users_Bool_Exp = {
  _and?: Maybe<Array<Users_Bool_Exp>>;
  _not?: Maybe<Users_Bool_Exp>;
  _or?: Maybe<Array<Users_Bool_Exp>>;
  email?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Int_Comparison_Exp>;
  interface?: Maybe<Int_Comparison_Exp>;
  password?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "Users" */
export enum Users_Constraint {
  /** unique or primary key constraint */
  UsersInterfaceEmailKey = 'Users_interface_email_key',
  /** unique or primary key constraint */
  UsersPkey = 'Users_pkey',
}

/** input type for incrementing numeric columns in table "Users" */
export type Users_Inc_Input = {
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "Users" */
export type Users_Insert_Input = {
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Users_Max_Fields = {
  __typename?: 'Users_max_fields';
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate min on columns */
export type Users_Min_Fields = {
  __typename?: 'Users_min_fields';
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** response of any mutation on the table "Users" */
export type Users_Mutation_Response = {
  __typename?: 'Users_mutation_response';
  /** number of rows affected by the mutation */
  affected_rows: Scalars['Int'];
  /** data from the rows affected by the mutation */
  returning: Array<Users>;
};

/** on conflict condition type for table "Users" */
export type Users_On_Conflict = {
  constraint: Users_Constraint;
  update_columns: Array<Users_Update_Column>;
  where?: Maybe<Users_Bool_Exp>;
};

/** Ordering options when selecting data from "Users". */
export type Users_Order_By = {
  email?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  interface?: Maybe<Order_By>;
  password?: Maybe<Order_By>;
};

/** primary key columns input for table: Users */
export type Users_Pk_Columns_Input = {
  id: Scalars['Int'];
};

/** select columns of table "Users" */
export enum Users_Select_Column {
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Password = 'password',
}

/** input type for updating data in table "Users" */
export type Users_Set_Input = {
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Users_Stddev_Fields = {
  __typename?: 'Users_stddev_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_pop on columns */
export type Users_Stddev_Pop_Fields = {
  __typename?: 'Users_stddev_pop_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate stddev_samp on columns */
export type Users_Stddev_Samp_Fields = {
  __typename?: 'Users_stddev_samp_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate sum on columns */
export type Users_Sum_Fields = {
  __typename?: 'Users_sum_fields';
  id?: Maybe<Scalars['Int']>;
  interface?: Maybe<Scalars['Int']>;
};

/** update columns of table "Users" */
export enum Users_Update_Column {
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  Interface = 'interface',
  /** column name */
  Password = 'password',
}

/** aggregate var_pop on columns */
export type Users_Var_Pop_Fields = {
  __typename?: 'Users_var_pop_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate var_samp on columns */
export type Users_Var_Samp_Fields = {
  __typename?: 'Users_var_samp_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

/** aggregate variance on columns */
export type Users_Variance_Fields = {
  __typename?: 'Users_variance_fields';
  id?: Maybe<Scalars['Float']>;
  interface?: Maybe<Scalars['Float']>;
};

export type LoginOutput = {
  __typename?: 'loginOutput';
  token: Scalars['String'];
};

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "EvemtPasswords" */
  delete_EvemtPasswords?: Maybe<EvemtPasswords_Mutation_Response>;
  /** delete single row from the table: "EvemtPasswords" */
  delete_EvemtPasswords_by_pk?: Maybe<EvemtPasswords>;
  /** delete data from the table: "EventPermissions" */
  delete_EventPermissions?: Maybe<EventPermissions_Mutation_Response>;
  /** delete single row from the table: "EventPermissions" */
  delete_EventPermissions_by_pk?: Maybe<EventPermissions>;
  /** delete data from the table: "Events" */
  delete_Events?: Maybe<Events_Mutation_Response>;
  /** delete single row from the table: "Events" */
  delete_Events_by_pk?: Maybe<Events>;
  /** delete data from the table: "InterfaceSubdomains" */
  delete_InterfaceSubdomains?: Maybe<InterfaceSubdomains_Mutation_Response>;
  /** delete single row from the table: "InterfaceSubdomains" */
  delete_InterfaceSubdomains_by_pk?: Maybe<InterfaceSubdomains>;
  /** delete data from the table: "InterfaceUrls" */
  delete_InterfaceUrls?: Maybe<InterfaceUrls_Mutation_Response>;
  /** delete single row from the table: "InterfaceUrls" */
  delete_InterfaceUrls_by_pk?: Maybe<InterfaceUrls>;
  /** delete data from the table: "Interfaces" */
  delete_Interfaces?: Maybe<Interfaces_Mutation_Response>;
  /** delete single row from the table: "Interfaces" */
  delete_Interfaces_by_pk?: Maybe<Interfaces>;
  /** delete data from the table: "OrganizationImages" */
  delete_OrganizationImages?: Maybe<OrganizationImages_Mutation_Response>;
  /** delete single row from the table: "OrganizationImages" */
  delete_OrganizationImages_by_pk?: Maybe<OrganizationImages>;
  /** delete data from the table: "OrganizationPermissions" */
  delete_OrganizationPermissions?: Maybe<OrganizationPermissions_Mutation_Response>;
  /** delete single row from the table: "OrganizationPermissions" */
  delete_OrganizationPermissions_by_pk?: Maybe<OrganizationPermissions>;
  /** delete data from the table: "Organizations" */
  delete_Organizations?: Maybe<Organizations_Mutation_Response>;
  /** delete single row from the table: "Organizations" */
  delete_Organizations_by_pk?: Maybe<Organizations>;
  /** delete data from the table: "Profiles" */
  delete_Profiles?: Maybe<Profiles_Mutation_Response>;
  /** delete single row from the table: "Profiles" */
  delete_Profiles_by_pk?: Maybe<Profiles>;
  /** delete data from the table: "Users" */
  delete_Users?: Maybe<Users_Mutation_Response>;
  /** delete single row from the table: "Users" */
  delete_Users_by_pk?: Maybe<Users>;
  /** insert data into the table: "EvemtPasswords" */
  insert_EvemtPasswords?: Maybe<EvemtPasswords_Mutation_Response>;
  /** insert a single row into the table: "EvemtPasswords" */
  insert_EvemtPasswords_one?: Maybe<EvemtPasswords>;
  /** insert data into the table: "EventPermissions" */
  insert_EventPermissions?: Maybe<EventPermissions_Mutation_Response>;
  /** insert a single row into the table: "EventPermissions" */
  insert_EventPermissions_one?: Maybe<EventPermissions>;
  /** insert data into the table: "Events" */
  insert_Events?: Maybe<Events_Mutation_Response>;
  /** insert a single row into the table: "Events" */
  insert_Events_one?: Maybe<Events>;
  /** insert data into the table: "InterfaceSubdomains" */
  insert_InterfaceSubdomains?: Maybe<InterfaceSubdomains_Mutation_Response>;
  /** insert a single row into the table: "InterfaceSubdomains" */
  insert_InterfaceSubdomains_one?: Maybe<InterfaceSubdomains>;
  /** insert data into the table: "InterfaceUrls" */
  insert_InterfaceUrls?: Maybe<InterfaceUrls_Mutation_Response>;
  /** insert a single row into the table: "InterfaceUrls" */
  insert_InterfaceUrls_one?: Maybe<InterfaceUrls>;
  /** insert data into the table: "Interfaces" */
  insert_Interfaces?: Maybe<Interfaces_Mutation_Response>;
  /** insert a single row into the table: "Interfaces" */
  insert_Interfaces_one?: Maybe<Interfaces>;
  /** insert data into the table: "OrganizationImages" */
  insert_OrganizationImages?: Maybe<OrganizationImages_Mutation_Response>;
  /** insert a single row into the table: "OrganizationImages" */
  insert_OrganizationImages_one?: Maybe<OrganizationImages>;
  /** insert data into the table: "OrganizationPermissions" */
  insert_OrganizationPermissions?: Maybe<OrganizationPermissions_Mutation_Response>;
  /** insert a single row into the table: "OrganizationPermissions" */
  insert_OrganizationPermissions_one?: Maybe<OrganizationPermissions>;
  /** insert data into the table: "Organizations" */
  insert_Organizations?: Maybe<Organizations_Mutation_Response>;
  /** insert a single row into the table: "Organizations" */
  insert_Organizations_one?: Maybe<Organizations>;
  /** insert data into the table: "Profiles" */
  insert_Profiles?: Maybe<Profiles_Mutation_Response>;
  /** insert a single row into the table: "Profiles" */
  insert_Profiles_one?: Maybe<Profiles>;
  /** insert data into the table: "Users" */
  insert_Users?: Maybe<Users_Mutation_Response>;
  /** insert a single row into the table: "Users" */
  insert_Users_one?: Maybe<Users>;
  login?: Maybe<LoginOutput>;
  signup?: Maybe<SignupOutput>;
  /** update data of the table: "EvemtPasswords" */
  update_EvemtPasswords?: Maybe<EvemtPasswords_Mutation_Response>;
  /** update single row of the table: "EvemtPasswords" */
  update_EvemtPasswords_by_pk?: Maybe<EvemtPasswords>;
  /** update data of the table: "EventPermissions" */
  update_EventPermissions?: Maybe<EventPermissions_Mutation_Response>;
  /** update single row of the table: "EventPermissions" */
  update_EventPermissions_by_pk?: Maybe<EventPermissions>;
  /** update data of the table: "Events" */
  update_Events?: Maybe<Events_Mutation_Response>;
  /** update single row of the table: "Events" */
  update_Events_by_pk?: Maybe<Events>;
  /** update data of the table: "InterfaceSubdomains" */
  update_InterfaceSubdomains?: Maybe<InterfaceSubdomains_Mutation_Response>;
  /** update single row of the table: "InterfaceSubdomains" */
  update_InterfaceSubdomains_by_pk?: Maybe<InterfaceSubdomains>;
  /** update data of the table: "InterfaceUrls" */
  update_InterfaceUrls?: Maybe<InterfaceUrls_Mutation_Response>;
  /** update single row of the table: "InterfaceUrls" */
  update_InterfaceUrls_by_pk?: Maybe<InterfaceUrls>;
  /** update data of the table: "Interfaces" */
  update_Interfaces?: Maybe<Interfaces_Mutation_Response>;
  /** update single row of the table: "Interfaces" */
  update_Interfaces_by_pk?: Maybe<Interfaces>;
  /** update data of the table: "OrganizationImages" */
  update_OrganizationImages?: Maybe<OrganizationImages_Mutation_Response>;
  /** update single row of the table: "OrganizationImages" */
  update_OrganizationImages_by_pk?: Maybe<OrganizationImages>;
  /** update data of the table: "OrganizationPermissions" */
  update_OrganizationPermissions?: Maybe<OrganizationPermissions_Mutation_Response>;
  /** update single row of the table: "OrganizationPermissions" */
  update_OrganizationPermissions_by_pk?: Maybe<OrganizationPermissions>;
  /** update data of the table: "Organizations" */
  update_Organizations?: Maybe<Organizations_Mutation_Response>;
  /** update single row of the table: "Organizations" */
  update_Organizations_by_pk?: Maybe<Organizations>;
  /** update data of the table: "Profiles" */
  update_Profiles?: Maybe<Profiles_Mutation_Response>;
  /** update single row of the table: "Profiles" */
  update_Profiles_by_pk?: Maybe<Profiles>;
  /** update data of the table: "Users" */
  update_Users?: Maybe<Users_Mutation_Response>;
  /** update single row of the table: "Users" */
  update_Users_by_pk?: Maybe<Users>;
};

/** mutation root */
export type Mutation_RootDelete_EvemtPasswordsArgs = {
  where: EvemtPasswords_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_EvemtPasswords_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_EventPermissionsArgs = {
  where: EventPermissions_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_EventPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_EventsArgs = {
  where: Events_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Events_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_InterfaceSubdomainsArgs = {
  where: InterfaceSubdomains_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_InterfaceSubdomains_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_InterfaceUrlsArgs = {
  where: InterfaceUrls_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_InterfaceUrls_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_InterfacesArgs = {
  where: Interfaces_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Interfaces_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_OrganizationImagesArgs = {
  where: OrganizationImages_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_OrganizationImages_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_OrganizationPermissionsArgs = {
  where: OrganizationPermissions_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_OrganizationPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_OrganizationsArgs = {
  where: Organizations_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Organizations_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_ProfilesArgs = {
  where: Profiles_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Profiles_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootDelete_UsersArgs = {
  where: Users_Bool_Exp;
};

/** mutation root */
export type Mutation_RootDelete_Users_By_PkArgs = {
  id: Scalars['Int'];
};

/** mutation root */
export type Mutation_RootInsert_EvemtPasswordsArgs = {
  objects: Array<EvemtPasswords_Insert_Input>;
  on_conflict?: Maybe<EvemtPasswords_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_EvemtPasswords_OneArgs = {
  object: EvemtPasswords_Insert_Input;
  on_conflict?: Maybe<EvemtPasswords_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_EventPermissionsArgs = {
  objects: Array<EventPermissions_Insert_Input>;
  on_conflict?: Maybe<EventPermissions_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_EventPermissions_OneArgs = {
  object: EventPermissions_Insert_Input;
  on_conflict?: Maybe<EventPermissions_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_EventsArgs = {
  objects: Array<Events_Insert_Input>;
  on_conflict?: Maybe<Events_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Events_OneArgs = {
  object: Events_Insert_Input;
  on_conflict?: Maybe<Events_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_InterfaceSubdomainsArgs = {
  objects: Array<InterfaceSubdomains_Insert_Input>;
  on_conflict?: Maybe<InterfaceSubdomains_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_InterfaceSubdomains_OneArgs = {
  object: InterfaceSubdomains_Insert_Input;
  on_conflict?: Maybe<InterfaceSubdomains_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_InterfaceUrlsArgs = {
  objects: Array<InterfaceUrls_Insert_Input>;
  on_conflict?: Maybe<InterfaceUrls_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_InterfaceUrls_OneArgs = {
  object: InterfaceUrls_Insert_Input;
  on_conflict?: Maybe<InterfaceUrls_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_InterfacesArgs = {
  objects: Array<Interfaces_Insert_Input>;
  on_conflict?: Maybe<Interfaces_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Interfaces_OneArgs = {
  object: Interfaces_Insert_Input;
  on_conflict?: Maybe<Interfaces_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_OrganizationImagesArgs = {
  objects: Array<OrganizationImages_Insert_Input>;
  on_conflict?: Maybe<OrganizationImages_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_OrganizationImages_OneArgs = {
  object: OrganizationImages_Insert_Input;
  on_conflict?: Maybe<OrganizationImages_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_OrganizationPermissionsArgs = {
  objects: Array<OrganizationPermissions_Insert_Input>;
  on_conflict?: Maybe<OrganizationPermissions_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_OrganizationPermissions_OneArgs = {
  object: OrganizationPermissions_Insert_Input;
  on_conflict?: Maybe<OrganizationPermissions_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_OrganizationsArgs = {
  objects: Array<Organizations_Insert_Input>;
  on_conflict?: Maybe<Organizations_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Organizations_OneArgs = {
  object: Organizations_Insert_Input;
  on_conflict?: Maybe<Organizations_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_ProfilesArgs = {
  objects: Array<Profiles_Insert_Input>;
  on_conflict?: Maybe<Profiles_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Profiles_OneArgs = {
  object: Profiles_Insert_Input;
  on_conflict?: Maybe<Profiles_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_UsersArgs = {
  objects: Array<Users_Insert_Input>;
  on_conflict?: Maybe<Users_On_Conflict>;
};

/** mutation root */
export type Mutation_RootInsert_Users_OneArgs = {
  object: Users_Insert_Input;
  on_conflict?: Maybe<Users_On_Conflict>;
};

/** mutation root */
export type Mutation_RootLoginArgs = {
  email: Scalars['String'];
  interface: Scalars['Int'];
  password: Scalars['String'];
};

/** mutation root */
export type Mutation_RootSignupArgs = {
  email?: Maybe<Scalars['String']>;
  interface?: Maybe<Scalars['Int']>;
  password?: Maybe<Scalars['String']>;
};

/** mutation root */
export type Mutation_RootUpdate_EvemtPasswordsArgs = {
  _inc?: Maybe<EvemtPasswords_Inc_Input>;
  _set?: Maybe<EvemtPasswords_Set_Input>;
  where: EvemtPasswords_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_EvemtPasswords_By_PkArgs = {
  _inc?: Maybe<EvemtPasswords_Inc_Input>;
  _set?: Maybe<EvemtPasswords_Set_Input>;
  pk_columns: EvemtPasswords_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_EventPermissionsArgs = {
  _inc?: Maybe<EventPermissions_Inc_Input>;
  _set?: Maybe<EventPermissions_Set_Input>;
  where: EventPermissions_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_EventPermissions_By_PkArgs = {
  _inc?: Maybe<EventPermissions_Inc_Input>;
  _set?: Maybe<EventPermissions_Set_Input>;
  pk_columns: EventPermissions_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_EventsArgs = {
  _inc?: Maybe<Events_Inc_Input>;
  _set?: Maybe<Events_Set_Input>;
  where: Events_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Events_By_PkArgs = {
  _inc?: Maybe<Events_Inc_Input>;
  _set?: Maybe<Events_Set_Input>;
  pk_columns: Events_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_InterfaceSubdomainsArgs = {
  _inc?: Maybe<InterfaceSubdomains_Inc_Input>;
  _set?: Maybe<InterfaceSubdomains_Set_Input>;
  where: InterfaceSubdomains_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_InterfaceSubdomains_By_PkArgs = {
  _inc?: Maybe<InterfaceSubdomains_Inc_Input>;
  _set?: Maybe<InterfaceSubdomains_Set_Input>;
  pk_columns: InterfaceSubdomains_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_InterfaceUrlsArgs = {
  _inc?: Maybe<InterfaceUrls_Inc_Input>;
  _set?: Maybe<InterfaceUrls_Set_Input>;
  where: InterfaceUrls_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_InterfaceUrls_By_PkArgs = {
  _inc?: Maybe<InterfaceUrls_Inc_Input>;
  _set?: Maybe<InterfaceUrls_Set_Input>;
  pk_columns: InterfaceUrls_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_InterfacesArgs = {
  _inc?: Maybe<Interfaces_Inc_Input>;
  _set?: Maybe<Interfaces_Set_Input>;
  where: Interfaces_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Interfaces_By_PkArgs = {
  _inc?: Maybe<Interfaces_Inc_Input>;
  _set?: Maybe<Interfaces_Set_Input>;
  pk_columns: Interfaces_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_OrganizationImagesArgs = {
  _inc?: Maybe<OrganizationImages_Inc_Input>;
  _set?: Maybe<OrganizationImages_Set_Input>;
  where: OrganizationImages_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_OrganizationImages_By_PkArgs = {
  _inc?: Maybe<OrganizationImages_Inc_Input>;
  _set?: Maybe<OrganizationImages_Set_Input>;
  pk_columns: OrganizationImages_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_OrganizationPermissionsArgs = {
  _inc?: Maybe<OrganizationPermissions_Inc_Input>;
  _set?: Maybe<OrganizationPermissions_Set_Input>;
  where: OrganizationPermissions_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_OrganizationPermissions_By_PkArgs = {
  _inc?: Maybe<OrganizationPermissions_Inc_Input>;
  _set?: Maybe<OrganizationPermissions_Set_Input>;
  pk_columns: OrganizationPermissions_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_OrganizationsArgs = {
  _inc?: Maybe<Organizations_Inc_Input>;
  _set?: Maybe<Organizations_Set_Input>;
  where: Organizations_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Organizations_By_PkArgs = {
  _inc?: Maybe<Organizations_Inc_Input>;
  _set?: Maybe<Organizations_Set_Input>;
  pk_columns: Organizations_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_ProfilesArgs = {
  _inc?: Maybe<Profiles_Inc_Input>;
  _set?: Maybe<Profiles_Set_Input>;
  where: Profiles_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Profiles_By_PkArgs = {
  _inc?: Maybe<Profiles_Inc_Input>;
  _set?: Maybe<Profiles_Set_Input>;
  pk_columns: Profiles_Pk_Columns_Input;
};

/** mutation root */
export type Mutation_RootUpdate_UsersArgs = {
  _inc?: Maybe<Users_Inc_Input>;
  _set?: Maybe<Users_Set_Input>;
  where: Users_Bool_Exp;
};

/** mutation root */
export type Mutation_RootUpdate_Users_By_PkArgs = {
  _inc?: Maybe<Users_Inc_Input>;
  _set?: Maybe<Users_Set_Input>;
  pk_columns: Users_Pk_Columns_Input;
};

/** column ordering options */
export enum Order_By {
  /** in ascending order, nulls last */
  Asc = 'asc',
  /** in ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in descending order, nulls first */
  Desc = 'desc',
  /** in descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in descending order, nulls last */
  DescNullsLast = 'desc_nulls_last',
}

export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "EvemtPasswords" */
  EvemtPasswords: Array<EvemtPasswords>;
  /** fetch aggregated fields from the table: "EvemtPasswords" */
  EvemtPasswords_aggregate: EvemtPasswords_Aggregate;
  /** fetch data from the table: "EvemtPasswords" using primary key columns */
  EvemtPasswords_by_pk?: Maybe<EvemtPasswords>;
  /** fetch data from the table: "EventPermissions" */
  EventPermissions: Array<EventPermissions>;
  /** fetch aggregated fields from the table: "EventPermissions" */
  EventPermissions_aggregate: EventPermissions_Aggregate;
  /** fetch data from the table: "EventPermissions" using primary key columns */
  EventPermissions_by_pk?: Maybe<EventPermissions>;
  /** fetch data from the table: "Events" */
  Events: Array<Events>;
  /** fetch aggregated fields from the table: "Events" */
  Events_aggregate: Events_Aggregate;
  /** fetch data from the table: "Events" using primary key columns */
  Events_by_pk?: Maybe<Events>;
  /** fetch data from the table: "InterfaceSubdomains" */
  InterfaceSubdomains: Array<InterfaceSubdomains>;
  /** fetch aggregated fields from the table: "InterfaceSubdomains" */
  InterfaceSubdomains_aggregate: InterfaceSubdomains_Aggregate;
  /** fetch data from the table: "InterfaceSubdomains" using primary key columns */
  InterfaceSubdomains_by_pk?: Maybe<InterfaceSubdomains>;
  /** fetch data from the table: "InterfaceUrls" */
  InterfaceUrls: Array<InterfaceUrls>;
  /** fetch aggregated fields from the table: "InterfaceUrls" */
  InterfaceUrls_aggregate: InterfaceUrls_Aggregate;
  /** fetch data from the table: "InterfaceUrls" using primary key columns */
  InterfaceUrls_by_pk?: Maybe<InterfaceUrls>;
  /** fetch data from the table: "Interfaces" */
  Interfaces: Array<Interfaces>;
  /** fetch aggregated fields from the table: "Interfaces" */
  Interfaces_aggregate: Interfaces_Aggregate;
  /** fetch data from the table: "Interfaces" using primary key columns */
  Interfaces_by_pk?: Maybe<Interfaces>;
  /** fetch data from the table: "OrganizationImages" */
  OrganizationImages: Array<OrganizationImages>;
  /** fetch aggregated fields from the table: "OrganizationImages" */
  OrganizationImages_aggregate: OrganizationImages_Aggregate;
  /** fetch data from the table: "OrganizationImages" using primary key columns */
  OrganizationImages_by_pk?: Maybe<OrganizationImages>;
  /** fetch data from the table: "OrganizationPermissions" */
  OrganizationPermissions: Array<OrganizationPermissions>;
  /** fetch aggregated fields from the table: "OrganizationPermissions" */
  OrganizationPermissions_aggregate: OrganizationPermissions_Aggregate;
  /** fetch data from the table: "OrganizationPermissions" using primary key columns */
  OrganizationPermissions_by_pk?: Maybe<OrganizationPermissions>;
  /** fetch data from the table: "Organizations" */
  Organizations: Array<Organizations>;
  /** fetch aggregated fields from the table: "Organizations" */
  Organizations_aggregate: Organizations_Aggregate;
  /** fetch data from the table: "Organizations" using primary key columns */
  Organizations_by_pk?: Maybe<Organizations>;
  /** fetch data from the table: "Profiles" */
  Profiles: Array<Profiles>;
  /** fetch aggregated fields from the table: "Profiles" */
  Profiles_aggregate: Profiles_Aggregate;
  /** fetch data from the table: "Profiles" using primary key columns */
  Profiles_by_pk?: Maybe<Profiles>;
  /** fetch data from the table: "Users" */
  Users: Array<Users>;
  /** fetch aggregated fields from the table: "Users" */
  Users_aggregate: Users_Aggregate;
  /** fetch data from the table: "Users" using primary key columns */
  Users_by_pk?: Maybe<Users>;
  refresh?: Maybe<RefreshOutput>;
};

export type Query_RootEvemtPasswordsArgs = {
  distinct_on?: Maybe<Array<EvemtPasswords_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EvemtPasswords_Order_By>>;
  where?: Maybe<EvemtPasswords_Bool_Exp>;
};

export type Query_RootEvemtPasswords_AggregateArgs = {
  distinct_on?: Maybe<Array<EvemtPasswords_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EvemtPasswords_Order_By>>;
  where?: Maybe<EvemtPasswords_Bool_Exp>;
};

export type Query_RootEvemtPasswords_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootEventPermissionsArgs = {
  distinct_on?: Maybe<Array<EventPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EventPermissions_Order_By>>;
  where?: Maybe<EventPermissions_Bool_Exp>;
};

export type Query_RootEventPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<EventPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EventPermissions_Order_By>>;
  where?: Maybe<EventPermissions_Bool_Exp>;
};

export type Query_RootEventPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Events_Order_By>>;
  where?: Maybe<Events_Bool_Exp>;
};

export type Query_RootEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Events_Order_By>>;
  where?: Maybe<Events_Bool_Exp>;
};

export type Query_RootEvents_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootInterfaceSubdomainsArgs = {
  distinct_on?: Maybe<Array<InterfaceSubdomains_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceSubdomains_Order_By>>;
  where?: Maybe<InterfaceSubdomains_Bool_Exp>;
};

export type Query_RootInterfaceSubdomains_AggregateArgs = {
  distinct_on?: Maybe<Array<InterfaceSubdomains_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceSubdomains_Order_By>>;
  where?: Maybe<InterfaceSubdomains_Bool_Exp>;
};

export type Query_RootInterfaceSubdomains_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootInterfaceUrlsArgs = {
  distinct_on?: Maybe<Array<InterfaceUrls_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceUrls_Order_By>>;
  where?: Maybe<InterfaceUrls_Bool_Exp>;
};

export type Query_RootInterfaceUrls_AggregateArgs = {
  distinct_on?: Maybe<Array<InterfaceUrls_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceUrls_Order_By>>;
  where?: Maybe<InterfaceUrls_Bool_Exp>;
};

export type Query_RootInterfaceUrls_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootInterfacesArgs = {
  distinct_on?: Maybe<Array<Interfaces_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Interfaces_Order_By>>;
  where?: Maybe<Interfaces_Bool_Exp>;
};

export type Query_RootInterfaces_AggregateArgs = {
  distinct_on?: Maybe<Array<Interfaces_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Interfaces_Order_By>>;
  where?: Maybe<Interfaces_Bool_Exp>;
};

export type Query_RootInterfaces_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootOrganizationImagesArgs = {
  distinct_on?: Maybe<Array<OrganizationImages_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationImages_Order_By>>;
  where?: Maybe<OrganizationImages_Bool_Exp>;
};

export type Query_RootOrganizationImages_AggregateArgs = {
  distinct_on?: Maybe<Array<OrganizationImages_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationImages_Order_By>>;
  where?: Maybe<OrganizationImages_Bool_Exp>;
};

export type Query_RootOrganizationImages_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootOrganizationPermissionsArgs = {
  distinct_on?: Maybe<Array<OrganizationPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationPermissions_Order_By>>;
  where?: Maybe<OrganizationPermissions_Bool_Exp>;
};

export type Query_RootOrganizationPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<OrganizationPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationPermissions_Order_By>>;
  where?: Maybe<OrganizationPermissions_Bool_Exp>;
};

export type Query_RootOrganizationPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootOrganizationsArgs = {
  distinct_on?: Maybe<Array<Organizations_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Organizations_Order_By>>;
  where?: Maybe<Organizations_Bool_Exp>;
};

export type Query_RootOrganizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Organizations_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Organizations_Order_By>>;
  where?: Maybe<Organizations_Bool_Exp>;
};

export type Query_RootOrganizations_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootProfilesArgs = {
  distinct_on?: Maybe<Array<Profiles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Profiles_Order_By>>;
  where?: Maybe<Profiles_Bool_Exp>;
};

export type Query_RootProfiles_AggregateArgs = {
  distinct_on?: Maybe<Array<Profiles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Profiles_Order_By>>;
  where?: Maybe<Profiles_Bool_Exp>;
};

export type Query_RootProfiles_By_PkArgs = {
  id: Scalars['Int'];
};

export type Query_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};

export type Query_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};

export type Query_RootUsers_By_PkArgs = {
  id: Scalars['Int'];
};

export type RefreshOutput = {
  __typename?: 'refreshOutput';
  token: Scalars['String'];
};

export type SignupOutput = {
  __typename?: 'signupOutput';
  id: Scalars['Int'];
  token: Scalars['String'];
};

export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "EvemtPasswords" */
  EvemtPasswords: Array<EvemtPasswords>;
  /** fetch aggregated fields from the table: "EvemtPasswords" */
  EvemtPasswords_aggregate: EvemtPasswords_Aggregate;
  /** fetch data from the table: "EvemtPasswords" using primary key columns */
  EvemtPasswords_by_pk?: Maybe<EvemtPasswords>;
  /** fetch data from the table: "EventPermissions" */
  EventPermissions: Array<EventPermissions>;
  /** fetch aggregated fields from the table: "EventPermissions" */
  EventPermissions_aggregate: EventPermissions_Aggregate;
  /** fetch data from the table: "EventPermissions" using primary key columns */
  EventPermissions_by_pk?: Maybe<EventPermissions>;
  /** fetch data from the table: "Events" */
  Events: Array<Events>;
  /** fetch aggregated fields from the table: "Events" */
  Events_aggregate: Events_Aggregate;
  /** fetch data from the table: "Events" using primary key columns */
  Events_by_pk?: Maybe<Events>;
  /** fetch data from the table: "InterfaceSubdomains" */
  InterfaceSubdomains: Array<InterfaceSubdomains>;
  /** fetch aggregated fields from the table: "InterfaceSubdomains" */
  InterfaceSubdomains_aggregate: InterfaceSubdomains_Aggregate;
  /** fetch data from the table: "InterfaceSubdomains" using primary key columns */
  InterfaceSubdomains_by_pk?: Maybe<InterfaceSubdomains>;
  /** fetch data from the table: "InterfaceUrls" */
  InterfaceUrls: Array<InterfaceUrls>;
  /** fetch aggregated fields from the table: "InterfaceUrls" */
  InterfaceUrls_aggregate: InterfaceUrls_Aggregate;
  /** fetch data from the table: "InterfaceUrls" using primary key columns */
  InterfaceUrls_by_pk?: Maybe<InterfaceUrls>;
  /** fetch data from the table: "Interfaces" */
  Interfaces: Array<Interfaces>;
  /** fetch aggregated fields from the table: "Interfaces" */
  Interfaces_aggregate: Interfaces_Aggregate;
  /** fetch data from the table: "Interfaces" using primary key columns */
  Interfaces_by_pk?: Maybe<Interfaces>;
  /** fetch data from the table: "OrganizationImages" */
  OrganizationImages: Array<OrganizationImages>;
  /** fetch aggregated fields from the table: "OrganizationImages" */
  OrganizationImages_aggregate: OrganizationImages_Aggregate;
  /** fetch data from the table: "OrganizationImages" using primary key columns */
  OrganizationImages_by_pk?: Maybe<OrganizationImages>;
  /** fetch data from the table: "OrganizationPermissions" */
  OrganizationPermissions: Array<OrganizationPermissions>;
  /** fetch aggregated fields from the table: "OrganizationPermissions" */
  OrganizationPermissions_aggregate: OrganizationPermissions_Aggregate;
  /** fetch data from the table: "OrganizationPermissions" using primary key columns */
  OrganizationPermissions_by_pk?: Maybe<OrganizationPermissions>;
  /** fetch data from the table: "Organizations" */
  Organizations: Array<Organizations>;
  /** fetch aggregated fields from the table: "Organizations" */
  Organizations_aggregate: Organizations_Aggregate;
  /** fetch data from the table: "Organizations" using primary key columns */
  Organizations_by_pk?: Maybe<Organizations>;
  /** fetch data from the table: "Profiles" */
  Profiles: Array<Profiles>;
  /** fetch aggregated fields from the table: "Profiles" */
  Profiles_aggregate: Profiles_Aggregate;
  /** fetch data from the table: "Profiles" using primary key columns */
  Profiles_by_pk?: Maybe<Profiles>;
  /** fetch data from the table: "Users" */
  Users: Array<Users>;
  /** fetch aggregated fields from the table: "Users" */
  Users_aggregate: Users_Aggregate;
  /** fetch data from the table: "Users" using primary key columns */
  Users_by_pk?: Maybe<Users>;
};

export type Subscription_RootEvemtPasswordsArgs = {
  distinct_on?: Maybe<Array<EvemtPasswords_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EvemtPasswords_Order_By>>;
  where?: Maybe<EvemtPasswords_Bool_Exp>;
};

export type Subscription_RootEvemtPasswords_AggregateArgs = {
  distinct_on?: Maybe<Array<EvemtPasswords_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EvemtPasswords_Order_By>>;
  where?: Maybe<EvemtPasswords_Bool_Exp>;
};

export type Subscription_RootEvemtPasswords_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootEventPermissionsArgs = {
  distinct_on?: Maybe<Array<EventPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EventPermissions_Order_By>>;
  where?: Maybe<EventPermissions_Bool_Exp>;
};

export type Subscription_RootEventPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<EventPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EventPermissions_Order_By>>;
  where?: Maybe<EventPermissions_Bool_Exp>;
};

export type Subscription_RootEventPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Events_Order_By>>;
  where?: Maybe<Events_Bool_Exp>;
};

export type Subscription_RootEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Events_Order_By>>;
  where?: Maybe<Events_Bool_Exp>;
};

export type Subscription_RootEvents_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootInterfaceSubdomainsArgs = {
  distinct_on?: Maybe<Array<InterfaceSubdomains_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceSubdomains_Order_By>>;
  where?: Maybe<InterfaceSubdomains_Bool_Exp>;
};

export type Subscription_RootInterfaceSubdomains_AggregateArgs = {
  distinct_on?: Maybe<Array<InterfaceSubdomains_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceSubdomains_Order_By>>;
  where?: Maybe<InterfaceSubdomains_Bool_Exp>;
};

export type Subscription_RootInterfaceSubdomains_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootInterfaceUrlsArgs = {
  distinct_on?: Maybe<Array<InterfaceUrls_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceUrls_Order_By>>;
  where?: Maybe<InterfaceUrls_Bool_Exp>;
};

export type Subscription_RootInterfaceUrls_AggregateArgs = {
  distinct_on?: Maybe<Array<InterfaceUrls_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<InterfaceUrls_Order_By>>;
  where?: Maybe<InterfaceUrls_Bool_Exp>;
};

export type Subscription_RootInterfaceUrls_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootInterfacesArgs = {
  distinct_on?: Maybe<Array<Interfaces_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Interfaces_Order_By>>;
  where?: Maybe<Interfaces_Bool_Exp>;
};

export type Subscription_RootInterfaces_AggregateArgs = {
  distinct_on?: Maybe<Array<Interfaces_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Interfaces_Order_By>>;
  where?: Maybe<Interfaces_Bool_Exp>;
};

export type Subscription_RootInterfaces_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootOrganizationImagesArgs = {
  distinct_on?: Maybe<Array<OrganizationImages_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationImages_Order_By>>;
  where?: Maybe<OrganizationImages_Bool_Exp>;
};

export type Subscription_RootOrganizationImages_AggregateArgs = {
  distinct_on?: Maybe<Array<OrganizationImages_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationImages_Order_By>>;
  where?: Maybe<OrganizationImages_Bool_Exp>;
};

export type Subscription_RootOrganizationImages_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootOrganizationPermissionsArgs = {
  distinct_on?: Maybe<Array<OrganizationPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationPermissions_Order_By>>;
  where?: Maybe<OrganizationPermissions_Bool_Exp>;
};

export type Subscription_RootOrganizationPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<OrganizationPermissions_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<OrganizationPermissions_Order_By>>;
  where?: Maybe<OrganizationPermissions_Bool_Exp>;
};

export type Subscription_RootOrganizationPermissions_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootOrganizationsArgs = {
  distinct_on?: Maybe<Array<Organizations_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Organizations_Order_By>>;
  where?: Maybe<Organizations_Bool_Exp>;
};

export type Subscription_RootOrganizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Organizations_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Organizations_Order_By>>;
  where?: Maybe<Organizations_Bool_Exp>;
};

export type Subscription_RootOrganizations_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootProfilesArgs = {
  distinct_on?: Maybe<Array<Profiles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Profiles_Order_By>>;
  where?: Maybe<Profiles_Bool_Exp>;
};

export type Subscription_RootProfiles_AggregateArgs = {
  distinct_on?: Maybe<Array<Profiles_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Profiles_Order_By>>;
  where?: Maybe<Profiles_Bool_Exp>;
};

export type Subscription_RootProfiles_By_PkArgs = {
  id: Scalars['Int'];
};

export type Subscription_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};

export type Subscription_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Users_Order_By>>;
  where?: Maybe<Users_Bool_Exp>;
};

export type Subscription_RootUsers_By_PkArgs = {
  id: Scalars['Int'];
};

/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamptz']>;
  _gt?: Maybe<Scalars['timestamptz']>;
  _gte?: Maybe<Scalars['timestamptz']>;
  _in?: Maybe<Array<Scalars['timestamptz']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamptz']>;
  _lte?: Maybe<Scalars['timestamptz']>;
  _neq?: Maybe<Scalars['timestamptz']>;
  _nin?: Maybe<Array<Scalars['timestamptz']>>;
};

export type ListUsersQueryVariables = Exact<{ [key: string]: never }>;

export type ListUsersQuery = { __typename?: 'query_root' } & {
  Users: Array<{ __typename?: 'Users' } & Pick<Users, 'id'>>;
};

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  interface: Scalars['Int'];
  password: Scalars['String'];
}>;

export type LoginMutation = { __typename?: 'mutation_root' } & {
  login?: Maybe<{ __typename?: 'loginOutput' } & Pick<LoginOutput, 'token'>>;
};

export type RefreshTokenQueryVariables = Exact<{ [key: string]: never }>;

export type RefreshTokenQuery = { __typename?: 'query_root' } & {
  refresh?: Maybe<
    { __typename?: 'refreshOutput' } & Pick<RefreshOutput, 'token'>
  >;
};

export type SignUpMutationVariables = Exact<{
  interface: Scalars['Int'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;

export type SignUpMutation = { __typename?: 'mutation_root' } & {
  signup?: Maybe<{ __typename?: 'signupOutput' } & Pick<SignupOutput, 'token'>>;
};

export const ListUsersDoc = gql`
  query ListUsers {
    Users {
      id
    }
  }
`;
export const LoginDoc = gql`
  mutation Login($email: String!, $interface: Int!, $password: String!) {
    login(email: $email, interface: $interface, password: $password) {
      token
    }
  }
`;
export const RefreshTokenDoc = gql`
  query RefreshToken {
    refresh {
      token
    }
  }
`;
export const SignUpDoc = gql`
  mutation SignUp($interface: Int!, $email: String!, $password: String!) {
    signup(interface: $interface, email: $email, password: $password) {
      token
    }
  }
`;
export const ListUsers = (
  options: Omit<QueryOptions<ListUsersQueryVariables>, 'query'>
): Readable<
  ApolloQueryResult<ListUsersQuery> & {
    query: ObservableQuery<ListUsersQuery, ListUsersQueryVariables>;
  }
> => {
  const q = client.watchQuery({
    query: ListUsersDoc,
    ...options,
  });
  var result = readable<
    ApolloQueryResult<ListUsersQuery> & {
      query: ObservableQuery<ListUsersQuery, ListUsersQueryVariables>;
    }
  >(
    { data: null, loading: true, error: null, networkStatus: 1, query: null },
    (set) => {
      q.subscribe((v) => {
        set({ ...v, query: q });
      });
    }
  );
  return result;
};

export const Login = (
  options: Omit<MutationOptions<any, LoginMutationVariables>, 'mutation'>
) => {
  const m = client.mutate<LoginMutation, LoginMutationVariables>({
    mutation: LoginDoc,
    ...options,
  });
  return m;
};
export const RefreshToken = (
  options: Omit<QueryOptions<RefreshTokenQueryVariables>, 'query'>
): Readable<
  ApolloQueryResult<RefreshTokenQuery> & {
    query: ObservableQuery<RefreshTokenQuery, RefreshTokenQueryVariables>;
  }
> => {
  const q = client.watchQuery({
    query: RefreshTokenDoc,
    ...options,
  });
  var result = readable<
    ApolloQueryResult<RefreshTokenQuery> & {
      query: ObservableQuery<RefreshTokenQuery, RefreshTokenQueryVariables>;
    }
  >(
    { data: null, loading: true, error: null, networkStatus: 1, query: null },
    (set) => {
      q.subscribe((v) => {
        set({ ...v, query: q });
      });
    }
  );
  return result;
};

export const SignUp = (
  options: Omit<MutationOptions<any, SignUpMutationVariables>, 'mutation'>
) => {
  const m = client.mutate<SignUpMutation, SignUpMutationVariables>({
    mutation: SignUpDoc,
    ...options,
  });
  return m;
};
